#pragma once

#include "any_of.hpp"

#include <algorithm>
#include <bitset>
#include <cctype>
#include <cstdint>
#include <iostream>
#include <queue>
#include <sstream>
#include <string>

namespace n2t {

	class Assembler {
	public:
		std::string buffer;
		
		Assembler(void);

		void clean(std::istream&);
		void assemble(std::istream&);

	private:
		uint16_t handleAInst(const std::string&, uint16_t&);
		uint16_t handleCInst(const std::string&);
	};

} // namespace n2t