#pragma once

#include <filesystem>
#include <fstream>
#include <iostream>

using path = std::filesystem::path;

namespace n2t {

class FileManager {
public:
	std::ifstream fin;
	std::ofstream fout;
	path inputPath, outputPath;

	FileManager(const path&);
	~FileManager(void);

	bool openInputFile(void);
	bool openOutputFile(void);
	void closeInputFile(void);
	void closeOutputFile(void);

private:
	bool isAsm(const path&);
};

} // namespace n2t