#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

void compare(std::istream&, std::istream&, const std::string&);

int main(int argc, char *argv[]) {
  std::string targetFile, refFile;
  if (argc != 3) {
    std::cerr << "Incorrect syntax. Usage: " << argv[0]
              << " targetfilename referencefilename"
              << std::endl;
    exit(1);
  }
  else {
    targetFile = argv[1];
    refFile = argv[2];
  }

  // Open target file
  std::ifstream ftarget;
  ftarget.open(targetFile);
  if (ftarget.fail()) {
    std::cerr << "Failed to open target file." << std::endl;
    exit(1);
  }

  // Open reference file
  std::ifstream fref;
  fref.open(refFile);
  if (fref.fail()) {
    std::cerr << "Failed to open reference file." << std::endl;
    exit(1);
  }

  // Compare target and reference files
  compare(ftarget, fref, targetFile);

  // Optional success message
  std::cout << "Comparison ended successfully!" << std::endl;

  // Close file streams
  ftarget.close();
  fref.close();
}

void compare(std::istream& target, std::istream& ref, const std::string& file) {
  std::ostringstream os;				// Initialize an output stream
  std::string tline, rline;				// Strings for holding individual lines
  int i = 1;							// Line counter
  while (std::getline(target, tline)) { // For each line in the target file:
    std::getline(ref, rline);           // Get the same line in the reference file
    if (tline.compare(rline) != 0) {	// If both strings are not equal, return an error
      std::cerr
          << "Comparison failed in " << file << " line " << i          // Specify file name and line number where comparison failed
          << ":\n\tTarget:    " << tline << "\n\tReference: " << rline // Show target and reference strings so user can investigate the error
          << std::endl;
      exit(2);
    }
    i++; // Increment line counter
  }
}