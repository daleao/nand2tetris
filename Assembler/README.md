# Hack Assembler
### Nand2Tetris Part 1 - Week 06

C++ implementation of a full assembler for the Hack machine architecture.

The assembler works in two passes: the first removes spaces and comments and populates the symbol table with jump labels; the second handles actual translation of each line from assembly to binary machine code.

Included a script to compare text files, to validate the assembler's output against .cmp files provided in the course material.