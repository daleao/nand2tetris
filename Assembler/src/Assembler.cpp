#include "../incl/Assembler.hpp"
#include "../incl/maps.hpp"

n2t::Assembler::Assembler() : buffer("") {}

void n2t::Assembler::clean(std::istream& is) {
    std::ostringstream oss; // Initialize an output stream
    uint16_t i = 0;			// Line number counter for use with symbolic labels

	for (std::string line; getline(is >> std::ws, line);) {													  // For each line of the input file ignoring indentation:
		line.erase(std::remove_if(line.begin(), line.end(), [](unsigned char x) { return isspace(x); }), line.end()); // Move whitespaces to the end and erase them
        
        switch (line[0]) { // Now look at the first character in each line
        	case '/': {	   // If that character is '/', this line is a comment
				break;	   // Do not pass it to the output stream
        	}
        	case '(': {														// If that character is '(', this line contains a label
        		n2t::indexFromSymb[line.substr(1, line.find(')') - 1)] = i;	// Add it to the symbol table ignoring the parentheses and its value is the current line number
        		break;														// Do not pass it to the output stream
        	}
        	default: {								   // Otherwise, this line contains an instruction
        		line = line.substr(0, line.find('/')); // Remove in-line comments
        		oss << line << "\n";				   // Pass it to the output stream
        		i++;								   // Increment line counter (only if a line was passed to the output)
        	}
        }
    }

    buffer = oss.str(); // Flush output stream into buffer
}

void n2t::Assembler::assemble(std::istream& is) {
    std::ostringstream oss; // Initialize an output stream
    uint16_t addr = 16;		// RAM address counter for storing symbolic variables
    
	for (std::string line; getline(is, line);) {								 // For each line of assembly code
		if (line[0] == '@') {														 // If first character is '@' then this line contains an A-instruction
			oss << std::bitset<16>(n2t::Assembler::handleAInst(line, addr)) << "\n"; // Call A-instruction handler, convert integer output to a binary string and pass to the output stream 
		} else {																	 // Otherwise, this line is necessarily a C-instruction
			oss << std::bitset<16>(n2t::Assembler::handleCInst(line)) << "\n";		 // Call C-instruction handler and do the same
		}
	}

	buffer = oss.str(); // Flush output stream into buffer
}

uint16_t n2t::Assembler::handleAInst(const std::string& s, uint16_t& addr) {
    uint16_t result = 0;							// Initialize output
    std::string temp = s.substr(1, s.length() - 1); // Placeholder susbtring ignoring '@' handle
        
    if (isdigit(s[1])) {		 // If second character in line is a digit,
		result += stoi(temp);	 // Convert string to integer and pass to output
		if (result > UINT16_MAX / 2 - 1) {
			result -= UINT16_MAX / 2 - 1;
		}
    } else {					 // Otherwise, second character in line is alphabetical meaning this line contains a variable declaration
        if (!indexFromSymb.count(temp)) { // Check symbol table; if it's not there, include it
            indexFromSymb[temp] = addr++; // Assign an address and then increment the address variable
        }
        result += indexFromSymb.at(temp); // Symbols already have an integer value, so simply pass it to output
    }

    return result;
}

uint16_t n2t::Assembler::handleCInst(const std::string& s) {
    std::string dest, comp, jump; // Parsed substrings (dest, comp and jump)
    std::string temp;			  // Placeholder substring
    size_t i, j;				  // Placeholders for delimiter positions 
	uint16_t result = 0;		  // Initialize output
        
    // First, check for a dest instruction
    if ((temp = s.substr(0, (i = s.find('=')))) != s) { // If line contains '=', then necessarily it is the first non-alphabetical character in the line and is preceded by a dest instruction
        dest = temp;
        i++;											// Beginning of next instruction piece
    } else {											// If '=' was not found, substring is equivalent to original string, so the line does not contain a dest instruction
        i = 0;											// Beginning of next instruction piece
    }

    // Next, every line necessarily contains a comp instruction, defined between '=' and ';'
    comp = s.substr(i, (j = s.find(';')) - i);

    // Finally, check for a jump instruction
    if (j != std::string::npos) { // If 'j' returned 'npos', this line does not contain a jump instruction
        jump = s.substr(j + 1);
    }

    // Bit-wise shift each instruction to its correct location
    result = 7 << 13;											// C-instruction begins with 0b111 (i.e. 7) and will be followed by 13 more bits
	result += bitsFromComp.at(comp) << 6;						// Add the 7 comp bits, which will be followed by 6 more bits
	if (!dest.empty()) result += bitsFromDest.at(dest) << 3;	// Add the 3 dest bits, which will be followed by 3 more bits
	if (!jump.empty()) result += bitsFromJump.at(jump);			// Add the final 3 jump bits
        
    return result;
}