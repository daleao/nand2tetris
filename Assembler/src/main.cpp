/*
 * Hack Assembler
 *	Created by Matheus L.
 */

#include "../incl/Assembler.hpp"
#include "../incl/FileManager.hpp"

#include <iostream>

int main(int argc, char* argv[]) {
	// Assert correct usage
	if (2 > argc || argc > 4) {
		std::cerr << "Bad syntax. Usage:\n\t" << argv[0] << " <input file>" << std::endl;
		exit(1);
	}

	// Instantiate workers
	n2t::FileManager fm(argv[1]);
	n2t::Assembler avengers; // wait for it...

	// Open input file
	bool openedInput = fm.openInputFile();
	if (!openedInput) exit(1);

	// Open output file
	bool openedOutput = fm.openOutputFile();
	if (!openedOutput) exit(1);
	
	// First pass (clear whitespaces, comments and handle labels)
	avengers.buffer.clear();
	avengers.clean(fm.fin);
	std::istringstream iss(avengers.buffer);

	// Second pass (translation)
	avengers.buffer.clear();
	avengers.assemble(iss);	// we did it guys!
	fm.fout << avengers.buffer;

	std::cout << "File " << fm.inputPath.filename() << " successfully translated." << std::endl;
}