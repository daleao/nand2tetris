#include "../incl/FileManager.hpp"

n2t::FileManager::FileManager(const path& path)
	: inputPath(path), outputPath(path) {
	outputPath.replace_extension("hack");
}

n2t::FileManager::~FileManager() {
	closeInputFile();
	closeOutputFile();
}

bool n2t::FileManager::openInputFile(void) {
	if (!isAsm(inputPath)) {
		std::cerr << "Wrong input extension." << std::endl;
		return false;
	}
	fin.open(inputPath);
	if (fin.fail()) {
		std::cerr << "Failed to open input file " << inputPath << std::endl;
		return false;
	}
	return true;
}

bool n2t::FileManager::openOutputFile(void) {
	fout.open(outputPath);
	if (fout.fail()) {
		std::cerr << "Failed to open output file " << outputPath << std::endl;
		return false;
	}
	return true;
}

void n2t::FileManager::closeInputFile(void) {
	if (fin.is_open()) {
		fin.close();
	}
	fin.clear();
}

void n2t::FileManager::closeOutputFile(void) {
	if (fout.is_open()) fout.close();
	fout.clear();
}

bool n2t::FileManager::isAsm(const path& p) {
	try {
		if (p.has_extension() && p.extension() == ".asm") return true;
	}
	catch (std::filesystem::filesystem_error & e) {
		std::cerr << e.what() << std::endl;
	}
	return false;
}