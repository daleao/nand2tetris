#pragma once

#include "lib/any_of.hpp"
#include "structs.hpp"

#include <algorithm>
#include <ctime>
#include <sstream>
#include <string>

namespace n2t {

	class ICodeWriter {
	public:
		ICodeWriter(const std::string&, bool);

		void callSysInit(void);
		void writeCommand(Instruction&);
		void endWriting(void);

		std::string& buffer(void);

	private:
		std::string _buffer;
		std::ostringstream _oss;
		const std::string _programName;
		std::string _moduleName, _functionName;
		std::string _globalCallLabel, _globalReturnLabel;
		int _labelIndex, _ROMIndex;
		bool _writeCall, _writeReturn;
		bool _writeComments;

		template <typename head_t, typename... tail_t>
		void write(const head_t& head, const tail_t&... tail);
		void write(void);

		void writeLoad(const std::string &, const std::string&);
		void writePush(void);
		void writePushC(const std::string&);
		void writePop(void);
		void writeCopy(const std::string&);
		void writeCopyC(const std::string&, const std::string&);
		void writeOp(const std::string&);
		void writeOpM(const std::string&, const std::string&);
		void writeAdd(const std::string&, const std::string&);
		void writeAddM(const std::string&, const std::string&);
		void writeSub(const std::string&, const std::string&);
		void writeSubM(const std::string&, const std::string&);
		void writeAnd(const std::string&);
		void writeAndM(const std::string&);
		void writeOr(const std::string&);
		void writeOrM(const std::string&);
		void writeNand(const std::string&);
		void writeNandM(const std::string&);
		void writeNor(const std::string&);
		void writeNorM(const std::string&);
		void writeComp(std::string&);
		void writeLabel(const std::string&);
		void writeJump(const std::string&);
		void writeIf(std::string&, const std::string&);
		void writeFunction(const std::string&);
		void writeCall(const std::string&);
		void writeReturn(void);
		void writeGlobalCall(void);
		void writeGlobalReturn(void);

		void setModule(const std::string&);
		void setFunction(const std::string&);
		void toUpper(std::string&);
	};

} // namespace n2t