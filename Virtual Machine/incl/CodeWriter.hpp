#pragma once

#include "lib/any_of.hpp"
#include "structs.hpp"

#include <algorithm>
#include <ctime>
#include <string>
#include <sstream>
#include <vector>

namespace n2t {

	class CodeWriter {
	public:
		CodeWriter(const std::string&, bool, bool);

		void callSysInit(void);
		void writeCommand(Instruction&);
		void endWriting(void);

		std::string& buffer(void);
		
	private:
		std::string _buffer;
		std::ostringstream _oss;
		const std::string _programName;
		std::string _moduleName, _functionName;
		std::string _globalCallLabel, _globalReturnLabel;
		int _labelIndex, _ROMIndex;
		bool _writeCall, _writeReturn;
		bool _optimize, _writeComments;
		
		template <typename head_t, typename... tail_t>
		void write(const head_t&, const tail_t&...);
		void write(void);

		void initializeStackPointer(void);
		void accessStack(void);
		void incrementStackPointer(void);
		void decrementStackPointer(void);
		void incrementStackPointerAndAccess(void);
		void decrementStackPointerAndAccess(void);
		void getSingleValueFromStack(void);
		void getTwoValuesFromStack(void);
		void compare(const std::string&);
		void optimizedCompare(const std::string&);

		void writeAdd(void);
		void writeOptimizedAdd(void);
		void writeSub(void);
		void writeOptimizedSub(void);
		void writeNeg(void);
		void writeOptimizedNeg(void);

		void writeAnd(void);
		void writeOptimizedAnd(void);
		void writeOr(void);
		void writeOptimizedOr(void);
		void writeNot(void);
		void writeOptimizedNot(void);

		void writeEq(void);
		void writeOptimizedEq(void);
		void writeGt(void);
		void writeOptimizedGt(void);
		void writeLt(void);
		void writeOptimizedLt(void);

		void writePush(const std::string&);
		void writeOptimizedPush(const std::string&);
		void writePop(const std::string&);
		void writeOptimizedPop(const std::string&);

		void writeLabel(const std::string&);
		void writeGoto(const std::string&);
		void writeIf(const std::string&);
		void writeOptimizedIf(const std::string&);

		void writeFunction(const std::string&);
		void writeOptimizedFunction(const std::string&);
		void writeCall(const std::string&);
		void writeGlobalCall(void);
		void writeOptimizedCall(const std::string&);
		void writeGlobalOptimizedCall(void);
		void writeReturn(void);
		void writeGlobalReturn(void);
		void writeGlobalOptimizedReturn(void);

		void setModule(const std::string&);
		void setFunction(const std::string&);
		void toUpper(std::string&);
	};

} // namespace n2t