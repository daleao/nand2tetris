#pragma once

#include "lib/any_of.hpp"
#include "lib/remove_indices.hpp"
#include "structs.hpp"

#include <cstdint>
#include <queue>
#include <sstream>
#include <string>
#include <unordered_map>

namespace n2t {

	class Interpreter {
	public:
		Interpreter(void);

		void interpret(Instruction&);
		void prune(void);

		std::string& buffer(void);
		std::deque<Instruction>& deque(void);

	private:
		std::string _buffer;
		std::deque<Instruction> _dq;
		std::string _functionName;

		void queuePush(const std::string&);
		void queuePop(const std::string&);
		void queueAdd(void);
		void queueSub(void);
		void queueNeg(void);
		void queueAnd(void);
		void queueOr(void);
		void queueNot(void);
		void queueEq(void);
		void queueGt(void);
		void queueLt(void);
		void queueLabel(const std::string&);
		void queueGoto(const std::string&);
		void queueIf(std::string&);
		void queueFunction(const std::string&);
		void queueCall(const std::string&);
		void queueReturn(void);
		void queueModule(const std::string&);

		void negate(std::string&);
		void toBuffer(void);
	};

} // namespace n2t