#pragma once

#include <tuple>

namespace ml {

	struct TagAnyOf {};

	template <typename... Args>
	std::tuple<TagAnyOf, Args...> any_of(Args&&... args) {
	   return std::tuple<TagAnyOf, Args...>(TagAnyOf(), std::forward<Args>(args)...);
	}

	template <class X, class Tuple, size_t Index, size_t ReverseIndex>
	struct CompareToTuple {
		static bool compare(const X& x, const Tuple& tuple) {
			return x == std::get<Index>(tuple) || CompareToTuple<X, Tuple, Index+1, ReverseIndex-1>::compare(x, tuple);
		}
	};

	template <class X, class Tuple, size_t Index>
	struct CompareToTuple<X, Tuple, Index, 0> {
		static bool compare(const X& x, const Tuple& tuple) {
			return false;
		}
	};

	template <typename X, typename... Args>
	bool operator==(const X& x, const std::tuple<TagAnyOf, Args...>& any) {
		typedef std::tuple<TagAnyOf, Args...> any_of_type;
		return CompareToTuple<X, any_of_type, 1, std::tuple_size<any_of_type>::value-1>::compare(x, any);
	}

} // namespace ml