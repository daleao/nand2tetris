#pragma once

#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace n2t {

	template <typename T>
	class Graph {
	public:
		Graph(void);
		void addNode(const T&);
		void addEdge(const T&, const T&);
		std::unordered_set<T> buildTreeFrom(const T&);

	private:
		std::unordered_map<T, std::vector<T>> _adjList;
	};

} // namespace n2t

#include "Graph.ipp"