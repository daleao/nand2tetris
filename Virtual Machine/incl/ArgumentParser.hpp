#pragma once

#include "lib/any_of.hpp"

#include <string>
#include <tuple>
#include <vector>
#include <unordered_set>

namespace n2t {

	class ArgumentParser {
	public:
		ArgumentParser(int, char**);

		void parseArguments(void);
		bool parameterExists(const std::string&) const;
		const std::string& getParameter(const std::string&) const;

		std::string& input(void);

	private:
		std::string _input;
		std::vector<std::string> _tokens;
		const std::unordered_set<std::string> _knownParams = {
			"--optimization", "-o"
		};
		const std::unordered_set<std::string> _knownFlags = {
			"--help", "-h",
			"--write-comments", "-wc",
			"--kill-dead", "-kd",
			"--save-iinterpreted", "-si"
		};
	};

} // namespace n2t