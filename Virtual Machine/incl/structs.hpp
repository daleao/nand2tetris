#pragma once

#include "lib/any_of.hpp"

#include <iomanip>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>

namespace n2t {

	struct Operand {
		std::string seg;
		int idx;

		Operand(void);
		Operand(const std::string&);
		Operand(int);
		bool isEmpty(void) const;

		void operator=(const Operand&);
		bool operator==(const Operand&) const;
		friend std::ostream& operator<<(std::ostream&, const Operand&);
	};

	struct Address {
		std::string seg;
		int idx;

		Address(void);
		Address(const std::string&);
		bool isEmpty(void) const;

		void operator=(const Address&);
		bool operator==(const Address&) const;
		friend std::ostream& operator<<(std::ostream&, const Address&);
	};

	struct Instruction {
		std::string opcode, what, where, how;

		Instruction(void);
		Instruction(const std::string&);
		std::string toString(void) const;

		void operator=(const Instruction&);
		bool operator==(const Instruction&) const;
	};

} // namespace n2t