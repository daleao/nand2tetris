#pragma once

#include "Graph.hpp"

#include <queue>
#include <string>
#include <sstream>
#include <unordered_set>

namespace n2t {

	class Parser {
	public:
		std::string buffer;

		VmParser(void);
		void enqueue(std::istream&);
		void parse(bool)
	private:
		std::deque<std::string> _deque;
		std::unordered_set<std::string> _calledFunctions;
		Graph<std::string> _callerGraph;

		void buildFunctionCallGraph();

	};

} // namespace n2t