#pragma once

#include <string>
#include <unordered_map>

namespace n2t {

	inline const std::unordered_map<std::string, std::string> segmentAbbreviations = {
		{ "local", "lcl" },    { "argument", "arg" }, { "this", "this" }, { "that", "that" },
		{ "constant", "cte" }, { "pointer", "ptr" },  { "temp", "tmp" },  { "static", "stc" }
	};

	inline const std::unordered_map<std::string, int> keyFromOpcode = {
		{ "push", 0 },      { "pop", 1 },
		{ "add", 2 },       { "sub", 3 },   { "neg", 4 },
		{ "and", 5 },       { "or", 6 },    { "not", 7 },
		{ "eq", 8 },        { "gt", 9 },    { "lt", 10 },
		{ "label", 11 },    { "goto", 12 }, { "if-goto", 13 },
		{ "function", 14 }, { "call", 15 }, { "return", 16 },
		{ "module", 17 }
	};

	inline const std::unordered_map<std::string, int> keyFromIOpcode = {
		{ "d_load", 0 },    { "push_d", 1 }, { "push_c", 2 },
		{ "d_pop", 3},      { "copy_d", 4 }, { "copy_c", 5 },
		{ "d_op", 6 },      { "m_op", 7 },
		{ "d_add", 8 },     { "d_sub", 9 },
		{ "m_add", 10 },    { "m_sub", 11 },
		{ "d_and", 12 },    { "d_or", 13 },
		{ "m_and", 14 },    { "m_or", 15 },
		{ "d_nand", 16 },   { "d_nor", 17 },
		{ "m_nand", 18 },   { "m_nor", 19 },
		{ "d_comp", 20 },   { "--", 21 },    { "jump", 22 },   { "jumpif_d", 23 },
		{ "function", 24 }, { "call", 25 },  { "return", 26 },
		{ "module", 27 }
	};

	inline std::unordered_map<std::string, int> calledFunctions = { };

} // namespace n2t