#pragma once

#include <filesystem>
#include <fstream>
#include <iostream>
#include <queue>
#include <string>

namespace n2t {

using std::filesystem::path;

	class FileManager {
	public:
		FileManager(void);
		~FileManager(void);

		void setPath(const path&);
		void getFiles(void);
		bool openNextFile(void);
		bool openInterFile(void);
		bool openOutputFile(void);
		void closeInputFile(void);
		void closeInterFile(void);
		void closeOutputFile(void);

		path& inputPath(void);
		path& outputPath(void);
		path& interPath(void);
		std::ifstream& readInputFile(void);
		void writeOutputFile(const std::string&);
		void writeInterFile(const std::string&);
		std::string& inputFileName(void);

		bool includesOS(void);

	private:
		std::ifstream _inputFile;
		std::ofstream _outputFile, _interFile;
		std::deque<path> _files;
		path _inputPath, _outputPath, _interPath;
		std::string _inputFileName;
		bool _foundSys;

		bool isFile(const path&) const;
		bool isDirectory(const path&) const;
		bool isVm(const path&) const;
		int countFiles(const path&) const;
	};

} // namespace n2t