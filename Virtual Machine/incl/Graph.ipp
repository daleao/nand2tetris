template <typename T>
n2t::Graph<T>::Graph(void) {}

template <typename T>
void n2t::Graph<T>::addNode(const T& node) { if (!_adjList.contains(node)) _adjList.insert({ node, { } }); }

template <typename T>
void n2t::Graph<T>::addEdge(const T& from, const T& to) {
	if (!_adjList.contains(from)) _adjList.insert({ from, { to } });
	else {
		auto& adjacent = _adjList.at(from);
		if (std::find(adjacent.begin(), adjacent.end(), to) == adjacent.end()) adjacent.push_back(to);
	}
}

template <typename T>
std::unordered_set<T> n2t::Graph<T>::buildTreeFrom(const T& root) {
	std::deque<T> to_search = { root };
	std::vector<T> tree;
	while (!to_search.empty()) {
		auto& now_searching = to_search.front();
		auto& branches = _adjList.at(now_searching);
		if (!branches.empty()) {
			std::copy_if(branches.begin(), branches.end(), std::back_inserter(to_search), [tree](const auto& node) {
				return std::find(tree.begin(), tree.end(), node) == tree.end();
			});
		}
		tree.push_back(now_searching);
		to_search.pop_front();		
	}
	return std::unordered_set(tree.begin(), tree.end());
}