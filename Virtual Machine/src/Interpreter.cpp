#include "../incl/Interpreter.hpp"
#include "../incl/maps.hpp"

n2t::Interpreter::Interpreter(void)	: _buffer(""), _functionName("") {}

void n2t::Interpreter::interpret(Instruction& inst) {
	switch (keyFromOpcode.at(inst.opcode)) {
	case 0: { queuePush(inst.what); break; }
	case 1: { queuePop(inst.what); break; }
	case 2: { queueAdd(); break; }
	case 3: { queueSub(); break; }
	case 4: { queueNeg(); break; }
	case 5: { queueAnd(); break; }
	case 6: { queueOr(); break; }
	case 7: { queueNot(); break; }
	case 8: { queueEq(); break; }
	case 9: { queueGt(); break; }
	case 10: { queueLt(); break; }
	case 11: { queueLabel(inst.what); break; }
	case 12: { queueGoto(inst.what); break; }
	case 13: { queueIf(inst.what); break; }
	case 14: { queueFunction(inst.what); break; }
	case 15: { queueCall(inst.what); break; }
	case 16: { queueReturn(); break; }
	case 17: { queueModule(inst.what); break; }
	}
}

void n2t::Interpreter::prune(void) {
	if (_dq.front().opcode.front() == 'd') _dq.push_front(Instruction("push_d"));
	std::vector<int> to_remove;
	std::vector<std::string> void_functions;
	for (auto rit = _dq.rbegin(), too_far = _dq.rend(); rit != too_far; ++rit) {
		auto current_idx = std::distance(_dq.begin(), rit.base() - 1);
		auto& current = _dq.at(current_idx);
		if (current.opcode == "call") { // find and remove void function return values
			if (!void_functions.empty() and std::find(void_functions.begin(), void_functions.end(), current.what) != void_functions.end())
				to_remove.insert(to_remove.end(), { current_idx - 1, current_idx - 2 });			
			else {
				auto& after_next = _dq.at(current_idx - 2);
				if (after_next.opcode == "copy_d" and after_next.where == "tmp 0") {
					auto ptr_to_function = std::find_if(_dq.rbegin(), _dq.rend(), [current](const Instruction& ic) {
						return ic.opcode == "function" and ic.what.substr(0, ic.what.find(' ')) == current.what.substr(0, current.what.find(' '));
						});
					if (ptr_to_function == _dq.rend()) continue;
					auto function_idx = std::distance(_dq.begin(), ptr_to_function.base() - 1);
					auto return_idx = function_idx - 1;
					while (_dq.at(return_idx).opcode != "return") --return_idx;
					if (_dq.at(return_idx + 1).opcode == "copy_c" and _dq.at(return_idx + 1).what == "0") {
						to_remove.insert(to_remove.end(), { return_idx + 1, current_idx - 1, current_idx - 2 });
						void_functions.push_back((*rit).what);
					}
				}
			}
		}
		else if (current.opcode == ml::any_of("d_load", "d_loadc")) { // find and remove duplicate loads to d-register
			auto& next = _dq.at(current_idx - 1);
			if (next.opcode == "push_d" and current_idx - 2 > 0) {
				auto& after_that = _dq.at(current_idx - 2);
				if (after_that.opcode == current.opcode and after_that.what == current.what) {
					if (current.how.empty() and !after_that.how.empty()) {
						Instruction replace_with("d_op");
						replace_with.how = after_that.how;
						after_that = replace_with;
					}
					else if (!current.how.empty() and after_that.how.empty()) {
						Instruction replace_with("d_op");
						if (current.how.front() == '+') {
							uint16_t constant = uint16_t(0 - (uint16_t)std::stoi(current.how.substr(1, std::string::npos)));
							replace_with.how = '+' + std::to_string(constant);
						}
						else if (current.how.back() == '-') replace_with.how = current.how;
						after_that = replace_with;
					}
					else if (!current.how.empty() and !after_that.how.empty()) {
						uint16_t current_constant = 0, constant_after_that = 0;
						if (current.how.front() == '+') current_constant = std::stoi(current.how.substr(1, std::string::npos));
						else if (current.how.back() == '-') current_constant = std::stoi(current.how.substr(0, current.how.find('-')));
						if (after_that.how.front() == '+') constant_after_that = std::stoi(after_that.how.substr(1, std::string::npos));
						else if (after_that.how.back() == '-')	constant_after_that = std::stoi(after_that.how.substr(0, current.how.find('-')));
						Instruction replace_with("d_op");
						if ((current.how.front() == '+' and after_that.how.front() == '+') or 
							(current.how.back() == '-' and after_that.how.back() == '-')) {
							auto constant_sum = std::to_string(constant_after_that - current_constant);
							replace_with.how = '+' + constant_sum;
						}
						else {
							auto constant_sum = std::to_string(current_constant + constant_after_that);
							replace_with.how = constant_sum + '-';
						}
						after_that = replace_with;
					}
					else to_remove.push_back(current_idx - 2);
				}
				if (current.opcode == "d_loadc") {
					current.opcode = "push_c";
					if (std::find(to_remove.begin(), to_remove.end(), current_idx) == to_remove.end()) to_remove.push_back(current_idx - 1);
				}
			}
		}
		else if (current.opcode == ml::any_of("copy_d", "copy_c")) { // find and remove unecessary load to d-register after copy
			auto& next = _dq.at(current_idx - 1);
			if (next.opcode == "d_load" and current.where == next.what) {
				if (!next.how.empty()) {
					if (current_idx - 2 > 0) {
						auto& after_that = _dq.at(current_idx - 2);
						if (after_that.opcode == "jumpif_d" and next.how == std::to_string(UINT16_MAX) + '-') {
							negate(after_that.what);
							to_remove.push_back(current_idx - 1);
						}
					}
					else if (next.how == "+0") { to_remove.push_back(current_idx - 1); }
					else {
						Instruction replace_with("d_op");
						replace_with.how = next.how;
						next = replace_with;
					}
				}
				else to_remove.push_back(current_idx - 1);
			}
		}
		else if (current.opcode == ml::any_of("d_neg", "d_not")) {
			auto& next = _dq.at(current_idx - 1);
			if (next.opcode == current.opcode) to_remove.insert(to_remove.end(), { current_idx, current_idx - 1 });
			else {
				Instruction replace_with("d_op");
				replace_with.how = current.opcode == "d_neg" ? "0-" : std::to_string(UINT16_MAX) + '-';
				current = replace_with;
			}
		}
		else if (current.opcode == "function" and current.what == "Sys.init 0") { // remove return value from Sys.init
			while (_dq.at(current_idx).opcode != "return") if (--current_idx < 0) break;
			++current_idx;
			if (_dq.at(current_idx).opcode == "copy_c" and _dq.at(current_idx).what == "0") to_remove.push_back(current_idx);
		}
	}
	std::sort(to_remove.begin(), to_remove.end());
	ml::remove_indices(_dq, to_remove);
}

std::string& n2t::Interpreter::buffer(void) {
	toBuffer();
	return _buffer;
}

std::deque<n2t::Instruction>& n2t::Interpreter::deque(void) { return _dq; }

void n2t::Interpreter::queuePush(const std::string& what) {
	Instruction& previous = _dq.front();
	if (previous.opcode.front() == 'd') _dq.push_front(Instruction("push_d"));
	else if (previous.opcode.front() == 'm' and previous.where == what) return;
	if (what.find("cte") != std::string::npos) {
		Instruction next("d_loadc");
		next.what = what.substr(what.find(' ') + 1, std::string::npos);
		_dq.push_front(next);
	}
	else {
		Instruction next("d_load");
		next.what = what;
		_dq.push_front(next);
	}
}

void n2t::Interpreter::queuePop(const std::string& where) {
	Instruction& previous = _dq.front();
	if (previous.opcode == ml::any_of("copy_d", "copy_c") or previous.opcode.front() == 'm') _dq.push_front(Instruction("d_pop"));
	if (previous.opcode == "d_loadc") {
		Instruction next("copy_c");
		next.what = previous.what;
		next.where = where;
		_dq.front() = next;
	}
	else if (previous.opcode == "d_load") {
		if (previous.what == where) {
			Instruction next("m_op");
			next.where = where;
			next.how = previous.how;
			_dq.front() = next;
		}
		else {
			Instruction next("copy_d");
			next.where = where;
			_dq.push_front(next);
		}
	}
	else if (previous.opcode == ml::any_of("d_add", "d_sub", "d_and", "d_or", "d_nand", "d_nor")) {
		Instruction& before_that = _dq.at(1);
		if (before_that.opcode == "d_load") {
			if (previous.what == where or before_that.what == where) {
				Instruction next;
				if (previous.opcode == "d_add") next.opcode = "m_add";
				else if (previous.opcode == "d_sub") next.opcode = "m_sub";
				else if (previous.opcode == "d_and") next.opcode = "m_and";
				else if (previous.opcode == "d_or") next.opcode = "m_or";
				else if (previous.opcode == "d_nand") next.opcode = "m_nand";
				else if (previous.opcode == "d_nor") next.opcode = "m_nor";
				next.where = where;
				if (previous.what == where) {
					if (previous.how.empty() xor before_that.how.empty()) if (!previous.how.empty()) next.how = previous.how;
					else if (!previous.how.empty() and !before_that.how.empty()) {
						uint16_t previous_constant = 0, constant_before_that = 0;
						if (previous.how.front() == '+') previous_constant += (uint16_t)std::stoi(previous.how.substr(0, previous.how.find('-')));
						else if (previous.how.front() == '-') previous_constant += (uint16_t)std::stoi(previous.how.substr(1, std::string::npos));
						if (before_that.how.front() == '+') constant_before_that += (uint16_t)std::stoi(before_that.how.substr(0, before_that.how.find('-')));
						else if (before_that.how.front() == '-') constant_before_that += (uint16_t)std::stoi(before_that.how.substr(1, std::string::npos));
						auto constant_sum = std::to_string(previous_constant + constant_before_that);
						if (previous.how.front() == '+' and before_that.how.front() == '+') before_that.how = '+' + constant_sum;
						else if (previous.how.back() == '-' and before_that.how.back() == '-') {
							before_that.how = constant_sum + '-';
							next.how = "0-";
						}
						else if (previous.how.back() == '-') {
							before_that.how = '+' + constant_sum;
							next.how == "0-";
						}
						else if (before_that.how.back() == '-') before_that.how = constant_sum + '-';
					}
				}
				else if (before_that.what == where) {
					before_that.what = previous.what;
					if (previous.how.empty() xor before_that.how.empty()) {
						if (!previous.how.empty()) {
							before_that.how = previous.how;
							if (next.opcode == "m_sub") {
								uint16_t constant = 0;
								if (previous.how.front() == '+') {
									constant -= (uint16_t)std::stoi(previous.how.substr(1, std::string::npos));
									before_that.how = std::to_string(constant) + '-';
								}
								else if (previous.how.back() == '-') {
									constant -= (uint16_t)std::stoi(previous.how.substr(0, previous.how.find('-')));
									before_that.how = '+' + std::to_string(constant);
								}
							}
							next.how = "0-";
						}
						else if (!before_that.how.empty()) {
							next.how = before_that.how;
							if (next.opcode == "m_sub") {
								uint16_t constant = 0;
								if (before_that.how.front() == '+') {
									constant -= (uint16_t)std::stoi(before_that.how.substr(1, std::string::npos));
									next.how = std::to_string(constant) + '-';
								}
								else if (before_that.how.back() == '-') {
									constant -= (uint16_t)std::stoi(before_that.how.substr(0, before_that.how.find('-')));
									next.how = '+' + std::to_string(constant);
								}
							}
							before_that.how = "0-";
						}
					}
					else if (!previous.how.empty() and !before_that.how.empty()) {
						uint16_t previous_constant = 0, constant_before_that = 0;
						if (previous.how.front() == '+') previous_constant += (uint16_t)std::stoi(previous.how.substr(0, previous.how.find('-')));
						else if (previous.how.front() == '-') previous_constant += (uint16_t)std::stoi(previous.how.substr(1, std::string::npos));
						if (before_that.how.front() == '+') constant_before_that += (uint16_t)std::stoi(before_that.how.substr(0, before_that.how.find('-')));
						else if (before_that.how.front() == '-') constant_before_that += (uint16_t)std::stoi(before_that.how.substr(1, std::string::npos));
						auto constant_sum = std::to_string(previous_constant + constant_before_that);
						if (previous.how.front() == '+' and before_that.how.front() == '+') {
							if (next.opcode == "m_sub") {
								before_that.how = constant_sum + '-';
								next.how = "0-";
							}
							else {
								before_that.how = '+' + constant_sum;
								next.how = "";
							}
						}
						else if (previous.how.back() == '-' and before_that.how.back() == '-') {
							if (next.opcode == "m_sub") {
								before_that.how = '+' + constant_sum;
								next.how = "";
							}
							else {
								before_that.how = constant_sum + '-';
								next.how = "0-";
							}
						}
						else if (previous.how.back() == '-' and before_that.how.front() == '+') {
							if (next.opcode == "m_sub") {
								before_that.how = '+' + constant_sum;
								next.how = "0-";
							}
							else {
								before_that.how = constant_sum + '-';
								next.how = "";
							}
						}
						else if (previous.how.front() = '+' and before_that.how.back() == '-') {
							if (next.opcode == "m_sub") {
								before_that.how = constant_sum + '-';
								next.how = "";
							}
							else {
								before_that.how = '+' + constant_sum;
								next.how == "0-";
							}
						}
					}
				}
				previous = next;
			}
			else {
				Instruction next("copy_d");
				next.where = where;
				_dq.push_front(next);
			}
		}
		else {
			Instruction next("copy_d");
			next.where = where;
			_dq.push_front(next);
		}
	}
	else {
		Instruction next("copy_d");
		next.where = where;
		_dq.push_front(next);
	}
}

void n2t::Interpreter::queueAdd(void) {
	Instruction previous = _dq.front();
	if (previous.opcode == ml::any_of("d_load", "d_loadc") and previous.what != "ret") {
		_dq.pop_front();
		Instruction& before_that = _dq.front();
		if (before_that.opcode == "push_d") {
			_dq.pop_front();
			before_that = _dq.front();
		}
		else _dq.push_front(Instruction("d_pop"));
		if (previous.opcode == "d_loadc") {
			if (previous.what == "0") return;
			if (before_that.opcode == "d_loadc") before_that.what = std::to_string((uint16_t)std::stoi(previous.what) + (uint16_t)std::stoi(before_that.what));
			else if (!before_that.how.empty()) {
				uint16_t constant = (uint16_t)std::stoi(previous.what);
				if (before_that.how.front() == '+') {
					constant += (uint16_t)std::stoi(before_that.how.substr(1, std::string::npos));
					before_that.how.replace(1, before_that.how.length() - 1, std::to_string(constant));
				}
				else if (before_that.how.back() == '-') {
					constant += (uint16_t)std::stoi(before_that.how.substr(0, previous.how.find('-')));
					before_that.how.replace(0, before_that.how.length() - 1, std::to_string(constant));
				}
			}
			else before_that.how = '+' + previous.what;
			_dq.front() = before_that;
		}
		else {
			if (before_that.opcode == "d_loadc") {
				Instruction next("d_load");
				next.what = previous.what;
				next.how = '+' + before_that.what;
				_dq.front() = next;
			}
			else {
				Instruction next("d_add");
				next.what = previous.what;
				next.how = previous.how;
				_dq.push_front(next);
			}
		}
	}
	else if (previous.opcode == ml::any_of("d_add", "d_sub") or previous.what == "ret") {
		Instruction next("d_add");
		next.what = "pop";
		_dq.push_front(next);
	}
	else _dq.push_front(Instruction("d_add"));
}

void n2t::Interpreter::queueSub(void) {
	Instruction previous = _dq.front();
	if (previous.opcode == ml::any_of("d_load", "d_loadc") and previous.what != "ret") {
		_dq.pop_front();
		Instruction& before_that = _dq.front();
		if (before_that.opcode == "push_d") {
			_dq.pop_front();
			before_that = _dq.front();
		}
		else _dq.push_front(Instruction("d_pop"));
		if (previous.opcode == "d_loadc") {
			if (previous.what == "0") return;
			if (before_that.opcode == "d_loadc") before_that.what = std::to_string((uint16_t)std::stoi(before_that.what) - (uint16_t)std::stoi(previous.what));
			else if (!before_that.how.empty()) {
				uint16_t constant = (uint16_t)std::stoi(previous.what);
				if (before_that.how.front() == '+') {
					constant -= (uint16_t)std::stoi(before_that.how.substr(1, std::string::npos));
					before_that.how.replace(1, before_that.how.length() - 1, std::to_string(constant));
				}
				else if (before_that.how.back() == '-') {
					constant -= (uint16_t)std::stoi(before_that.how.substr(0, before_that.how.find('-')));
					before_that.how.replace(0, before_that.how.length() - 1, std::to_string(constant));
				}
			}
			else before_that.how = '+' + std::to_string((uint16_t)(0 - (uint16_t)std::stoi(previous.what)));
			_dq.front() = before_that;
		}
		else {
			if (before_that.opcode == "d_loadc") {
				Instruction next("d_load");
				next.what = previous.what;
				next.how = '+' + std::to_string((uint16_t)(0 - (uint16_t)std::stoi(before_that.what)));
				_dq.front() = next;
			}
			else {
				Instruction next("d_sub");
				next.what = previous.what;
				next.how = previous.how;
				_dq.push_front(next);
			}
		}
	}
	else if (previous.opcode == ml::any_of("d_add", "d_sub") or previous.what == "ret") {
		Instruction next("d_sub");
		next.what = "pop";
		_dq.push_front(next);
	}
	else _dq.push_front(Instruction("d_sub"));
}

void n2t::Interpreter::queueNeg(void) {
	Instruction& previous = _dq.front();
	if (previous.opcode == "d_loadc") {
		previous.what = std::to_string((uint16_t)(0 - (uint16_t)std::stoi(previous.what)));
	}
	else if (previous.opcode == "d_load") {
		if (!previous.how.empty()) {
			uint16_t constant = 0;
			if (previous.how.front() == '+') {
				constant -= (uint16_t)std::stoi(previous.how.substr(1, std::string::npos));
				previous.how = std::to_string(constant) + '-';
			}
			else if (previous.how.back() == '-') {
				constant -= (uint16_t)std::stoi(previous.how.substr(0, previous.how.find('-')));
				previous.how = '+' + std::to_string(constant);
			}
		}
		else previous.how = "0-";
	}
	else if (previous.opcode == ml::any_of("d_add", "d_sub")) {
		Instruction* iptr;
		size_t idx = 0;
		do {
			iptr = &_dq.at(idx++);
			if (iptr->opcode == "d_add") iptr->opcode = "d_sub";
			else if (iptr->opcode == "d_sub") iptr->opcode = "d_add";
			if (iptr->opcode == "d_load" or iptr->what == "pop") {
				if (!iptr->how.empty()) {
					uint16_t constant = 0;
					if (iptr->how.front() == '+') {
						constant -= (uint16_t)std::stoi(iptr->how.substr(1, std::string::npos));
						iptr->how = std::to_string(constant) + '-';
					}
					else if (iptr->how.back() == '-') {
						constant -= (uint16_t)std::stoi(iptr->how.substr(0, previous.how.find('-')));
						iptr->how = '+' + std::to_string(constant);
					}
				}
				else iptr->how = "0-";
			}
		} while (iptr->opcode != "d_load" or iptr->what != "pop");
	}
	else _dq.push_front(Instruction("d_neg"));
}

void n2t::Interpreter::queueAnd(void) {
	Instruction previous = _dq.front();
	if (previous.opcode == ml::any_of("d_load", "d_loadc") and previous.what != "ret") {
		_dq.pop_front();
		if (_dq.front().opcode == "push_d") _dq.pop_front();
		else _dq.push_front(Instruction("d_pop"));
		if (previous.opcode == "d_loadc") {
			Instruction& before_that = _dq.front();
			if (before_that.opcode == "d_loadc") {
				before_that.what = std::to_string((uint16_t)std::stoi(before_that.what) & (uint16_t)std::stoi(previous.what));
				return;
			}
		}
		Instruction next;
		if (!previous.how.empty() and previous.how.back() == '-') {
			queueNot();
			next.opcode = "d_nor";
			next.how = "+0";
		}
		else next.opcode = "d_and";
		next.what = previous.opcode == "d_loadc" ? "cte " + previous.what : previous.what;
		_dq.push_front(next);
	}
	else if (previous.opcode == ml::any_of("d_add", "d_sub", "d_comp") or previous.what == "ret") {
		Instruction next("d_and");
		next.what = "pop";
		_dq.push_front(next);
	}
	else _dq.push_front(Instruction("d_and"));
}

void n2t::Interpreter::queueOr(void) {
	Instruction previous = _dq.front();
	if (previous.opcode == ml::any_of("d_load", "d_loadc") and previous.what != "ret") {
		_dq.pop_front();
		if (_dq.front().opcode == "push_d") _dq.pop_front();
		else _dq.push_front(Instruction("d_pop"));
		if (previous.opcode == "d_loadc") {
			Instruction& before_that = _dq.front();
			if (before_that.opcode == "d_loadc") {
				before_that.what = std::to_string((uint16_t)std::stoi(before_that.what) | (uint16_t)std::stoi(previous.what));
				return;
			}
		}
		Instruction next;
		if (!previous.how.empty() and previous.how.back() == '-') {
			queueNot();
			next.opcode = "d_nor";
			next.how = "+0";
		}
		else next.opcode = "d_or";
		next.what = previous.opcode == "d_loadc" ? "cte " + previous.what : previous.what;
		_dq.push_front(next);
	}
	else if (previous.opcode == ml::any_of("d_and", "d_or", "d_comp") or previous.what == "ret") {
		Instruction next("d_or");
		next.what = "pop";
		_dq.push_front(next);
	}
	else _dq.push_front(Instruction("d_or"));
}

void n2t::Interpreter::queueNot(void) {
	Instruction& previous = _dq.front();
	if (previous.opcode == "d_loadc") previous.what = std::to_string((uint16_t)(UINT16_MAX - (uint16_t)std::stoi(previous.what)));
	else if (previous.opcode == "d_load") {
		if (!previous.how.empty()) {
			if (previous.how.front() == '+') previous.how = std::to_string(UINT16_MAX) + '-';
			else if (previous.how.back() == '-') previous.how = "+0";
		}
		else previous.how = std::to_string(UINT16_MAX) + '-';
	}
	else if (previous.opcode == "d_and") previous.opcode = "d_nand";
	else if (previous.opcode == "d_nand") previous.opcode = "d_and";
	else if (previous.opcode == "d_or") previous.opcode = "d_nor";
	else if (previous.opcode == "d_nor") previous.opcode = "d_or";
	else if (previous.opcode == "d_comp") negate(previous.what);
	else _dq.push_front(Instruction("d_not"));
}

void n2t::Interpreter::queueEq(void) {
	queueSub();
	Instruction& previous = _dq.front();
	if (previous.opcode == "d_loadc") {
		previous.what = previous.what == "0" ? std::to_string(UINT16_MAX) : "0";
		return;
	}
	Instruction next("d_comp");
	next.what = "eq";
	_dq.push_front(next);
}

void n2t::Interpreter::queueGt(void) {
	queueSub();
	Instruction& previous = _dq.front();
	if (previous.opcode == "d_loadc") {
		previous.what = std::stoi(previous.what) > 0 ? std::to_string(UINT16_MAX) : "0";
		return;
	}
	Instruction next("d_comp");
	next.what = "gt";
	_dq.push_front(next);
}

void n2t::Interpreter::queueLt(void) {
	queueSub();
	Instruction& previous = _dq.front();
	if (previous.opcode == "d_loadc") {
		previous.what = std::stoi(previous.what) < 0 ? std::to_string(UINT16_MAX) : "0";
		return;
	}
	Instruction next("d_comp");
	next.what = "lt";
	_dq.push_front(next);
}

void n2t::Interpreter::queueLabel(const std::string& what) {
	Instruction &previous = _dq.front(), &before_that = _dq.at(1);
	const auto label = _functionName + '$' + what;
	if (previous.opcode == "jump" and before_that.opcode == "jumpif_d" and before_that.where == label) {
		negate(before_that.what);
		before_that.where = previous.where;
		_dq.pop_front();
	}
	else {
		Instruction next("--");
		next.what = label;
		_dq.push_front(next);
	}
}

void n2t::Interpreter::queueGoto(const std::string& what) {
	Instruction next("jump");
	next.where = _functionName + '$' + what;;
	_dq.push_front(next);
}

void n2t::Interpreter::queueIf(std::string& what) {
	Instruction& previous = _dq.front();
	Instruction next("jumpif_d");
	next.where = _functionName + '$' + what;
	if (previous.opcode == "d_loadc" and previous.what == "0") { // Fix for dumb compiler (infinite loop)
		_dq.pop_front();
		return;
	}
	else if (previous.opcode == "d_comp") {
		next.what = previous.what;
		_dq.pop_front();
	}
	else if (previous.opcode == ml::any_of("d_load", "d_and", "d_or")) next.what = "ne";
	else if (previous.opcode == ml::any_of("d_nand", "d_nor")) {
		if (previous.opcode == "d_nand") previous.opcode = "d_and";
		else if (previous.opcode == "d_nor") previous.opcode = "d_or";
		next.what = "eq";
	}
	else next.what = "ne";
	_dq.push_front(next);
}

void n2t::Interpreter::queueFunction(const std::string& what) {
	_functionName = what.substr(0, what.find(' '));
	Instruction next("function");
	next.what = what;
	_dq.push_front(next);
}

void n2t::Interpreter::queueCall(const std::string& what) {
	Instruction& previous = _dq.front();
	Instruction next("call"), after_that("d_load");
	if (previous.opcode.front() == 'd') _dq.push_front(Instruction("push_d"));
	next.what = what;
	after_that.what = "ret";
	_dq.push_front(next);
	_dq.push_front(after_that);
}

void n2t::Interpreter::queueReturn(void) {
	Instruction& previous = _dq.front();
	if (previous.opcode == "d_loadc") {
		previous.opcode = "copy_c";
		previous.where = "ret";
	}
	else if (previous.what == "ret") _dq.pop_front();
	else {
		Instruction next("copy_d");
		next.where = "ret";
		_dq.push_front(next);
	}
	_dq.push_front(Instruction("return"));
}

void n2t::Interpreter::queueModule(const std::string& what) {
	Instruction next("module");
	next.what = what;
	_dq.push_front(next);
}

void n2t::Interpreter::negate(std::string& cond) {
	if (cond == "eq") cond = "ne";
	else if (cond == "gt") cond = "le";
	else if (cond == "lt") cond = "ge";
	else if (cond == "ne") cond = "eq";
	else if (cond == "le") cond = "gt";
	else if (cond == "ge") cond = "lt";
}

void n2t::Interpreter::toBuffer(void) {
	std::ostringstream oss;
	for (auto it = _dq.rbegin(), too_far = _dq.rend(); it != too_far; ++it) oss << (*it).toString() << "\n";
	_buffer = oss.str();
}