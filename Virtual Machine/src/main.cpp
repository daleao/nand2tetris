#include "../incl/ArgumentParser.hpp"
#include "../incl/CodeWriter.hpp"
#include "../incl/FileManager.hpp"
#include "../incl/Graph.hpp"
#include "../incl/ICodeWriter.hpp"
#include "../incl/Interpreter.hpp"
#include "../incl/lib/any_of.hpp"
#include "../incl/lib/remove_indices.hpp"
#include "../incl/structs.hpp"

#include <filesystem>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

void printUsage(const std::string&);
void killTheDead(std::deque<n2t::Instruction>&);

int main(int argc, char* argv[]) {
	if (argc < 2) {
		std::cerr << "Not enough arguments. For details, use parameter '--help'." << std::endl;
		exit(0);
	}

	/* Parse command line arguments and get parameters */
	n2t::ArgumentParser ap(argc, argv);
	if (ap.parameterExists("--help") or ap.parameterExists("-h")) printUsage(argv[0]);
	try {
		ap.parseArguments();
	}
	catch (const std::string& e) {
		std::cerr << e << std::endl;
		exit(1);
	}

	int optimization_level = 2;
	if (ap.parameterExists("--optimization") or ap.parameterExists("-o")) {
		auto param = ap.parameterExists("-o") ? "-o" : "--optimization";
		try {
			optimization_level = std::stoi(ap.getParameter(param));
			std::cout << "Optimization level set to " << optimization_level << ".\n" << std::endl;
		}
		catch (const std::string& msg) {
			std::cerr << msg << std::endl;
			exit(1);
		}
	}

	bool with_comments = false;
	if (ap.parameterExists("--with-comments") or ap.parameterExists("-wc")) with_comments = true;

	bool dont_dead_open_inside = false;
	if (ap.parameterExists("--kill-dead") or ap.parameterExists("-kd")) dont_dead_open_inside = true;
	
	bool save_icode = false;
	if (ap.parameterExists("--save-interpreted") or ap.parameterExists("-si")) save_icode = true;

	/* Prepare input and output files */
	n2t::FileManager fm;
	try {
		fm.setPath(ap.input());
		std::cout << "Getting file(s)..." << std::endl;
		fm.getFiles();
		std::cout << "Done.\n" << std::endl;
	}
	catch (const std::string & e) {
		std::cerr << e << std::endl;
		exit(1);
	}

	/* Read and merge files, ignoring empty lines and comments */
	std::deque<n2t::Instruction> dq;
	while (fm.openNextFile()) {
		dq.push_back(n2t::Instruction("module " + fm.inputFileName()));
		for (std::string line; std::getline(fm.readInputFile(), line);) {
			if (!line.empty() and line[0] != '/') {
				dq.push_back(n2t::Instruction(line));
			}
		}
	}

	/* Remove dead functions */
	if (dont_dead_open_inside) {
		std::cout << "Looking for dead functions..." << std::endl;
		killTheDead(dq);
		std::cout << "Done.\n" << std::endl;
	}

	/* Translate */
	std::string program = fm.inputPath().stem().string(), output;
	std::cout << "Translating program '" << program << "'..." << std::endl;
	if (optimization_level <= 1) {
		n2t::CodeWriter cw(program, (bool)optimization_level, with_comments);
		std::cout << "Writing assembly..." << std::endl;
		if (fm.includesOS()) cw.callSysInit();
		for (auto& inst : dq) cw.writeCommand(inst);
		cw.endWriting();
		output = cw.buffer();
	}
	else {
		n2t::Interpreter in;
		std::cout << "Generating interpreted code..." << std::endl;
		for (auto& inst : dq) in.interpret(inst);
		in.prune();

		if (optimization_level > 2) {
			// relative indexing goes here
		}

		if (save_icode) {
			std::cout << "Saving interpreted code..." << std::endl;
			bool openedInter = fm.openInterFile();
			if (!openedInter) std::cerr << "The interpreted code could not be saved." << std::endl;
			else {
				fm.writeInterFile(in.buffer());
				fm.closeInterFile();
				std::cout << "Saved to '" << fm.interPath().string() << "'." << std::endl;
			}
		}

		n2t::ICodeWriter cw(program, with_comments);
		std::cout << "Writing assembly..." << std::endl;
		if (fm.includesOS()) cw.callSysInit();
		for (auto it = in.deque().rbegin(); it != in.deque().rend(); ++it) cw.writeCommand(*it);
		cw.endWriting();
		output = cw.buffer();
	}
	std::cout << "Translation finished.\n\nSaving output..." << std::endl;

	/* Save output */
	if (!fm.openOutputFile()) exit(1);
	fm.writeOutputFile(output);
	std::cout << "Saved to '" << fm.outputPath().string() << "'." << std::endl;
}

void printUsage(const std::string& executable) {
	std::cout << "Usage:\n\t" << executable << " <input path>"
			  << "\nOptional flags:"
			  << std::left << std::setw(30) << "\n\t--optimization,-o" << "Optimization level from 0 to 3:"
			  << std::left << std::setw(30) << "\n\t" << "\t0\tNone"
			  << std::left << std::setw(30) << "\n\t" << "\t1\tBlind"
			  << std::left << std::setw(30) << "\n\t" << "\t2\tInterpreted"
			  << std::left << std::setw(30) << "\n\t" << "\t3\t+ Relative Indexing"
			  << std::left << std::setw(30) << "\n\t--write-comments,-wc" << "Comment output with corresponding VM commands"
			  << std::left << std::setw(30) << "\n\t--kill-dead,-kd" << "Remove unused functions (dead code removal)"
			  << std::left << std::setw(30) << "\n\t--save-interpreted,-si" << "Save intermediate interpreted code to separate file"
			  << std::endl;
	exit(0);
}

void killTheDead(std::deque<n2t::Instruction>& dq) {
	n2t::Graph<std::string> caller_graph;
	std::string caller, callee;
	for (const auto& inst : dq) {
		if (inst.opcode == "function") {
			caller = inst.what.substr(0, inst.what.find(' '));
			caller_graph.addNode(caller);
		}
		else if (inst.opcode == "call") {
			callee = inst.what.substr(0, inst.what.find(' '));
			caller_graph.addEdge(caller, callee);
		}
	}
	auto called = caller_graph.buildTreeFrom("Sys.init");
	std::vector<int> to_remove;
	for (auto it = dq.begin(); it != dq.end(); ++it) {
		if (it->opcode == "function") {
			auto function = it->what.substr(0, it->what.find(' '));
			if (!called.count(function)) {
				std::cout << "Removing dead function '" << function << "'." << std::endl;
				do to_remove.push_back(std::distance(dq.begin(), it++));
				while (it->opcode != ml::any_of("function", "module"));
				--it;
			}
		}
	}
	ml::remove_indices(dq, to_remove);
}