#include "../incl/ArgumentParser.hpp"

n2t::ArgumentParser::ArgumentParser(int argc, char** argv) {
	_tokens.assign(argv + 1, argv + argc);
}

void n2t::ArgumentParser::parseArguments(void) {
	if (_tokens.front().front() != '-') {
		_input = _tokens.front();
		_tokens.erase(_tokens.begin(), _tokens.begin() + 1);
	}
	else throw ("Missing input argument.");

	for (auto it = _tokens.begin(); it != _tokens.end(); ++it) {
		if (!_knownFlags.count(*it)) {
			if (!_knownParams.count(*it)) throw ("Unkown parameter '" + *it + "'.\nFor details, use '--help'.");
			else {
				if (++it == _tokens.end() or it->front() == '-') throw ("Missing value for parameter '" + *it + "'.");
			}
		}
	}
}

bool n2t::ArgumentParser::parameterExists(const std::string& par) const {
	return std::find(_tokens.begin(), _tokens.end(), par) != _tokens.end();
}

const std::string& n2t::ArgumentParser::getParameter(const std::string& par) const {
	auto it = std::find(_tokens.begin(), _tokens.end(), par);
	if (it != _tokens.end() and ++it != _tokens.end()) return _tokens.at(std::distance(_tokens.begin(), it));
	else throw ("Missing parameter value.");
}

std::string& n2t::ArgumentParser::input(void) {
	return _input;
}