﻿#include "../incl/CodeWriter.hpp"
#include "../incl/maps.hpp"

n2t::CodeWriter::CodeWriter(const std::string& program, bool optimize, bool writeComments)
	: _buffer(""), _programName(program), _optimize(optimize),  _writeComments(writeComments), _writeCall(false), _writeReturn(false), _ROMIndex(0) {
	srand(std::time(NULL));
	_labelIndex = rand() % 1000;
	_globalCallLabel = _programName + ".global$CALL" + std::to_string(++_labelIndex);
	_globalReturnLabel = _programName + ".global$RET" + std::to_string(++_labelIndex);
	write("// .......... PROGRAM: ", _programName, " ..........");
	if (_writeComments) write("\n// (", _ROMIndex, "): bootstrap code");
	initializeStackPointer();
}

void n2t::CodeWriter::writeCommand(Instruction& inst) {
	if (_writeComments and inst.opcode != "module") write("\n// (", _ROMIndex, "): ", inst.toString());
	switch (keyFromOpcode.at(inst.opcode)) {
	case 0: { _optimize ? writeOptimizedPush(inst.what) : writePush(inst.what); break; }
	case 1: { _optimize ? writeOptimizedPop(inst.what) : writePop(inst.what); break; }
	case 2: { _optimize ? writeOptimizedAdd() : writeAdd(); break; }
	case 3: { _optimize ? writeOptimizedSub() : writeSub(); break; }
	case 4: { _optimize ? writeOptimizedNeg() : writeNeg(); break; }
	case 5: { _optimize ? writeOptimizedAnd() : writeAnd(); break; }
	case 6: { _optimize ? writeOptimizedOr() : writeOr(); break; }
	case 7: { _optimize ? writeOptimizedNot() : writeNot(); break; }
	case 8: { _optimize ? writeOptimizedEq() : writeEq(); break; }
	case 9: { _optimize ? writeOptimizedGt() : writeGt(); break; }
	case 10: { _optimize ? writeOptimizedLt() : writeLt(); break; }
	case 11: { writeLabel(inst.what); break; }
	case 12: { writeGoto(inst.what); break; }
	case 13: { _optimize ? writeOptimizedIf(inst.what) : writeIf(inst.what); break; }
	case 14: { _optimize ? writeOptimizedFunction(inst.what) : writeFunction(inst.what); break; }
	case 15: { _optimize ? writeOptimizedCall(inst.what) : writeCall(inst.what); break; }
	case 16: { writeReturn(); break; }
	case 17: { setModule(inst.what); break; }
	}
}

template <typename head_t, typename... tail_t>
void n2t::CodeWriter::write(const head_t& head, const tail_t&... tail) {
	_oss << head;
	write(tail...);
}

void n2t::CodeWriter::write(void) { _oss << "\n"; }

void n2t::CodeWriter::callSysInit(void) {
	write("@Sys.init\n0;JMP");
	_ROMIndex += 2;
}

void n2t::CodeWriter::endWriting(void) {
	if (_writeCall) _optimize ? writeGlobalOptimizedCall() : writeGlobalCall();
	if (_writeReturn) _optimize ? writeGlobalOptimizedReturn() : writeGlobalReturn();
	_oss << std::endl;
	std::istringstream iss(_oss.str());
	std::ostringstream oss;
	for (std::string line; std::getline(iss, line);) {
		if (line[0] != ml::any_of('(', '/')) oss << "\t";
		oss << line << "\n";
	}
	_buffer = oss.str();
}

std::string& n2t::CodeWriter::buffer(void) {
	return _buffer;
}

void n2t::CodeWriter::initializeStackPointer(void) {
	_optimize ? write("@255") : write("@256");
	write("D=A\n@SP\nM=D");
	_ROMIndex += 4;
}

void n2t::CodeWriter::accessStack(void) { write("@SP\nA=M"); _ROMIndex += 2; }

void n2t::CodeWriter::incrementStackPointer(void) { write("@SP\nM=M+1"); _ROMIndex += 2; }

void n2t::CodeWriter::incrementStackPointerAndAccess(void) { write("@SP\nAM=M+1"); _ROMIndex += 2; }

void n2t::CodeWriter::decrementStackPointer(void) { write("@SP\nM=M-1"); _ROMIndex += 2; }

void n2t::CodeWriter::decrementStackPointerAndAccess(void) { write("@SP\nAM=M-1"); _ROMIndex += 2; }

void n2t::CodeWriter::getSingleValueFromStack(void) {
	decrementStackPointer();
	write("A=M"); _ROMIndex++;
}

void n2t::CodeWriter::getTwoValuesFromStack(void) {
	getSingleValueFromStack();
	write("D=M"); _ROMIndex++;
	getSingleValueFromStack();
}

void n2t::CodeWriter::compare(const std::string& condition) {
	const auto true_label = _moduleName + "$CMP_TRUE" + std::to_string(++_labelIndex);
	write("M=-1\n@", true_label, "\nD;J", condition); _ROMIndex += 3;
	accessStack();
	write("M=0\n(", true_label, ')'); _ROMIndex++;
}

void n2t::CodeWriter::optimizedCompare(const std::string& condition) {
	const auto true_label = _moduleName + "$CMP_TRUE" + std::to_string(++_labelIndex);
	const auto end_label = _moduleName + "$CMP_END" + std::to_string(_labelIndex);
	write('@', true_label, "\nD;J", condition, "\n@", end_label, "\nD=0;JMP\n(", true_label, ")\nD=-1\n(", end_label, ')'); _ROMIndex += 5;
	accessStack();
	write("M=D"); _ROMIndex++;
}

void n2t::CodeWriter::writeAdd(void) {
	getTwoValuesFromStack();
	write("M=D+M"); _ROMIndex++;
	incrementStackPointer();
}

void n2t::CodeWriter::writeOptimizedAdd(void) {
	decrementStackPointerAndAccess();
	write("MD=D+M"); _ROMIndex++;
}

void n2t::CodeWriter::writeSub(void) {
	getTwoValuesFromStack();
	write("M=M-D"); _ROMIndex++;
	incrementStackPointer();
}

void n2t::CodeWriter::writeOptimizedSub(void) {
	decrementStackPointerAndAccess();
	write("MD=M-D"); _ROMIndex++;
}

void n2t::CodeWriter::writeNeg(void) {
	getSingleValueFromStack();
	write("M=-M"); _ROMIndex++;
	incrementStackPointer();
}

void n2t::CodeWriter::writeOptimizedNeg(void) {
	write("MD=-M"); _ROMIndex++;
}

void n2t::CodeWriter::writeAnd(void) {
	getTwoValuesFromStack();
	write("M=D&M"); _ROMIndex++;
	incrementStackPointer();
}

void n2t::CodeWriter::writeOptimizedAnd(void) {
	decrementStackPointerAndAccess();
	write("MD=D&M"); _ROMIndex++;
}

void n2t::CodeWriter::writeOr(void) {
	getTwoValuesFromStack();
	write("M=D|M"); _ROMIndex++;
	incrementStackPointer();
}

void n2t::CodeWriter::writeOptimizedOr(void) {
	decrementStackPointerAndAccess();
	write("MD=D|M"); _ROMIndex++;
}

void n2t::CodeWriter::writeNot(void) {
	getSingleValueFromStack();
	write("M=!M"); _ROMIndex++;
	incrementStackPointer();
}

void n2t::CodeWriter::writeOptimizedNot(void) {
	write("MD=!M"); _ROMIndex++;
}

void n2t::CodeWriter::writeEq(void) {
	getTwoValuesFromStack();
	write("D=M-D"); _ROMIndex++;
	compare("EQ");
	incrementStackPointer();
}

void n2t::CodeWriter::writeOptimizedEq(void) {
	decrementStackPointerAndAccess();
	write("D=M-D"); _ROMIndex++;
	optimizedCompare("EQ");
}

void n2t::CodeWriter::writeGt(void) {
	getTwoValuesFromStack();
	write("D=M-D"); _ROMIndex++;
	compare("GT");
	incrementStackPointer();
}

void n2t::CodeWriter::writeOptimizedGt(void) {
	decrementStackPointerAndAccess();
	write("D=M-D"); _ROMIndex++;
	optimizedCompare("GT");
}

void n2t::CodeWriter::writeLt(void) {
	getTwoValuesFromStack();
	write("D=M-D"); _ROMIndex++;
	compare("LT");
	incrementStackPointer();
}

void n2t::CodeWriter::writeOptimizedLt(void) {
	decrementStackPointerAndAccess();
	write("D=M-D"); _ROMIndex++;
	optimizedCompare("LT");
}

void n2t::CodeWriter::writePush(const std::string& what) {
	Address from(what);
	if (from.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(from.seg);
		write('@', from.seg, "\nD=M\n@", from.idx, "\nA=D+A\nD=M");
		_ROMIndex += 5;
	}
	else if (from.seg == ml::any_of("ptr", "tmp")) {
		write("@R", from.seg == "ptr" ? "3" : "5");
		write("D=A\n@", from.idx, "\nA=D+A\nD=M");
		_ROMIndex += 5;
	}
	else if (from.seg == "cte") { write('@', from.idx, "\nD=A"); _ROMIndex += 2; }
	else if (from.seg == "stc") { write("@", _moduleName, ".", from.idx, "\nD=M"); _ROMIndex += 2; }
	accessStack();
	write("M=D"); _ROMIndex++;
	incrementStackPointer();
}

void n2t::CodeWriter::writeOptimizedPush(const std::string& what) {
	Address from(what);
	if (from.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(from.seg);
		write('@', from.seg); _ROMIndex ++;
		if (from.idx == 0) { write("A=M"); _ROMIndex++; }
		else if (from.idx == 1) { write("A=M+1"); _ROMIndex++; }
		else if (from.idx == 2) { write("A=M+1\nA=A+1"); _ROMIndex += 2; }
		else { write("D=M\n@", from.idx, "\nA=D+A"); _ROMIndex += 3; }
		write("D=M"); _ROMIndex++;
	}
	else if (from.seg == ml::any_of("ptr", "tmp")) {
		int base_idx = from.seg == "ptr" ? 3 : 5;
		write("@R", base_idx + from.idx, "\nD=M");
		_ROMIndex += 2;
	}
	else if (from.seg == "cte") {
		if (from.idx <= 1) {
			incrementStackPointerAndAccess();
			write("MD=", from.idx);
			_ROMIndex++;
			return;
		}
		else { write('@', from.idx, "\nD=A"); _ROMIndex += 2; }
	}
	else if (from.seg == "stc") { write("@", _moduleName, ".", from.idx, "\nD=M"); _ROMIndex += 2; }
	incrementStackPointerAndAccess();
	write("MD=D"); _ROMIndex++;
}

void n2t::CodeWriter::writePop(const std::string& where) {
	Address at(where);
	if (at.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(at.seg);
		write('@', at.seg, "\nD=M\n@", at.idx, "\nD=D+A");
		_ROMIndex += 4;
	}
	else if (at.seg == ml::any_of("ptr", "tmp")) {
		write("@R", at.seg == "ptr" ? "3" : "5");
		write("D=A\n@", at.idx, "\nD=D+A");
		_ROMIndex += 4;
	}
	else if (at.seg == "stc") { write("@", _moduleName, ".", at.idx, "\nD=A"); _ROMIndex += 2; }
	getSingleValueFromStack();
	write("D=D+M\nA=D-M\nM=D-A"); _ROMIndex += 3;
}

void n2t::CodeWriter::writeOptimizedPop(const std::string& where) {
	Address at(where);
	if (at.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(at.seg);
		if (at.idx <= 3) {
			decrementStackPointerAndAccess();
			write("A=A+1\nD=M\n@", at.seg);
			if (at.idx == 0) { write("A=M"); _ROMIndex++; }
			else if (at.idx == 1) { write("A=M+1"); _ROMIndex++; }
			else if (at.idx == 2) { write("A=M+1\nA=A+1"); _ROMIndex += 2; }
			else if (at.idx == 3) { write("A=M+1\nA=A+1\nA=A+1"); _ROMIndex += 3; }
			write("M=D"); _ROMIndex += 4;
		}
		else {
			write('@', at.seg, "\nD=M\n@", at.idx, "\nD=D+A");
			decrementStackPointerAndAccess();
			write("A=A+1\nD=D+M\nA=D-M\nM=D-A");
			_ROMIndex += 8;
		}
	}
	else if (at.seg == ml::any_of("ptr", "tmp", "stc")) {
		int base_idx = at.seg == "ptr" ? 3 : 5;
		decrementStackPointerAndAccess();
		write("A=A+1\nD=M");
		at.seg == "stc" ? write('@', _moduleName, '.', at.idx) : write("@R", base_idx + at.idx);
		write("M=D");
		_ROMIndex += 4;
	}
}

void n2t::CodeWriter::writeLabel(const std::string& what) { write('(', _functionName + '$' + what, ')'); }

void n2t::CodeWriter::writeGoto(const std::string& where) { 
	write('@', _functionName + '$' + where, "\n0;JMP");
	_ROMIndex += 2;
}

void n2t::CodeWriter::writeIf(const std::string& where) {
	getSingleValueFromStack();
	write("D=M\n@", _functionName, '$', where, "\nD;JNE");
	_ROMIndex += 3;
}

void n2t::CodeWriter::writeOptimizedIf(const std::string& where) {
	decrementStackPointer();
	write('@', _functionName, '$', where, "\nD;JNE");
	_ROMIndex += 2;
}

void n2t::CodeWriter::writeFunction(const std::string& what) {
	auto pos = what.find(' ');
	auto function = what.substr(0, pos);
	auto nvars = std::stoi(what.substr(pos + 1, std::string::npos));
	setFunction(function);
	write('(', _functionName, ')');
	for (int i = 0; i < nvars; i++) writePush("cte 0");
}

void n2t::CodeWriter::writeOptimizedFunction(const std::string& what) {
	auto pos = what.find(' ');
	auto function = what.substr(0, pos);
	auto nvars = std::stoi(what.substr(pos + 1, std::string::npos));
	setFunction(function);
	write('(', _functionName, ')');
	if (nvars > 3) {
		const std::string loop = _functionName + "$INIT" + std::to_string(++_labelIndex);
		write('@', nvars, "\nD=A\n@R13\nM=D\n(", loop, ')');
		writeOptimizedPush("cte 0");
		write("@R13\nMD=M-1\n@", loop, "\nD;JNE");
		_ROMIndex += 8;
		return;
	}
	else for (int i = 0; i < nvars; i++) writeOptimizedPush("cte 0");
}

void n2t::CodeWriter::writeCall(const std::string& what) {
	if (!_writeCall) _writeCall = true;
	auto pos = what.find(' ');
	auto function = what.substr(0, pos);
	auto nargs = std::stoi(what.substr(pos + 1, std::string::npos));
	const std::string return_label = _functionName + "$RET" + std::to_string(++_labelIndex);
	write('@', function, "\nD=A\n@R14\nM=D");
	write('@', nargs, "\nD=A\n@R15\nM=D");
	write('@', return_label, "\nD=A");
	write('@', _globalCallLabel, "\n0;JMP\n(", return_label, ')');
	_ROMIndex += 12;
}

void n2t::CodeWriter::writeGlobalCall(void) {
	if (_writeComments) write("\n// (", _ROMIndex, "): global call subroutine");
	write('(', _globalCallLabel, ')');
	accessStack();
	write("M=D");
	incrementStackPointer();
	for (const std::string& seg : { "LCL", "ARG", "THIS", "THAT" }) {
		write('@', seg, "\nD=M");
		accessStack();
		write("M=D");
		incrementStackPointer();
		_ROMIndex += 3;
	}
	write("@SP\nD=M\n@R15\nD=D-M\n@5\nD=D-A\n@ARG\nM=D\n@SP\nD=M\n@LCL\nM=D\n@R14\nA=M\n0;JMP");
	_ROMIndex += 16;
}

void n2t::CodeWriter::writeOptimizedCall(const std::string& what) {
	if (!_writeCall) _writeCall = true;
	auto pos = what.find(' ');
	auto function = what.substr(0, pos);
	auto nargs = std::stoi(what.substr(pos + 1, std::string::npos));
	const std::string return_label = _functionName + "$RET" + std::to_string(++_labelIndex);
	write('@', function, "\nD=A\n@R14\nM=D");
	if (nargs == 0) { write("@R15\nM=0"); _ROMIndex += 2; }
	else if (nargs == 1) { write("@R15\nM=1"); _ROMIndex += 2; }
	else if (nargs == 2) { write("@R15\nM=1\nM=M+1"); _ROMIndex += 3; }
	else { write('@', nargs, "\nD=A\n@R15\nM=D"); _ROMIndex += 4; }
	write('@', return_label, "\nD=A"); 
	write('@', _globalCallLabel, "\n0;JMP\n(", return_label, ')');
	_ROMIndex += 8;
}

void n2t::CodeWriter::writeGlobalOptimizedCall(void) {
	if (_writeComments) write("\n// (", _ROMIndex, "): global call subroutine");
	write('(', _globalCallLabel, ')');
	write("@SP\nAM=M+1\nM=D");
	for (auto&& seg : { "LCL", "ARG", "THIS", "THAT" }) {
		write('@', seg, "\nD=M");
		incrementStackPointerAndAccess();
		write("M=D");
		_ROMIndex += 3;
	}
	write("D=A\n@LCL\nM=D+1\n@R15\nD=D-M\n@4\nD=D-A\n@ARG\nM=D\n@R14\nA=M\n0;JMP");
	_ROMIndex += 15;
}

void n2t::CodeWriter::writeReturn(void) {
	if (!_writeReturn) _writeReturn = true;
	write('@', _globalReturnLabel, "\n0;JMP");
	_ROMIndex += 2;
}

void n2t::CodeWriter::writeGlobalReturn(void) {
	if (_writeComments) write("\n// (", _ROMIndex, "): global return subroutine");
	write('(', _globalReturnLabel, ')');
	write("@LCL\nD=M\n@R13\nM=D\n@5\nA=D-A\nD=M\n@R14\nM=D");
	writePop("arg 0");
	write("@ARG\nD=M+1\n@SP\nM=D");
	int offset = 1;
	for (auto&& seg : { "THAT", "THIS", "ARG", "LCL" }) { write("@R13\nD=M\n@", offset++, "\nA=D-A\nD=M\n@", seg, "\nM=D"); _ROMIndex += 7; }
	write("@R14\nA=M\n0;JMP");
	_ROMIndex += 16;
}

void n2t::CodeWriter::writeGlobalOptimizedReturn(void) {
	if (_writeComments) write("\n// (", _ROMIndex, "): global return subroutine");
	write('(', _globalReturnLabel, ')');
	write("@5\nD=A\n@LCL\nA=M-D\nD=M\n@R13\nM=D");
	writeOptimizedPop("arg 0");
	write("D=A\n@SP\nM=D\n@LCL\nD=M\n@R14\nAM=D-1\nD=M\n@THAT\nM=D");
	for (auto&& seg : { "THIS", "ARG", "LCL" }) { write("@R14\nAM=M-1\nD=M\n@", seg, "\nM=D"); _ROMIndex += 5; }
	accessStack();
	write("D=M\n@R13\nA=M\n0;JMP");
	_ROMIndex += 21;
}

void n2t::CodeWriter::setModule(const std::string& module) {
	if (_writeComments) write("\n// ... Module: ", module, " ...");
	_moduleName = module;
}

void n2t::CodeWriter::setFunction(const std::string& function) { _functionName = function; }

void n2t::CodeWriter::toUpper(std::string& s) {
	std::for_each(s.begin(), s.end(), [](char& c) {
		c = ::toupper(c);
	});
}