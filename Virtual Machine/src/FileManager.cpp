#include "../incl/FileManager.hpp"

using path = std::filesystem::path;

n2t::FileManager::FileManager(void)
	: _inputPath(""), _outputPath(""), _interPath(""), _foundSys(false) {}

n2t::FileManager::~FileManager(void) {
	closeInputFile();
	closeInterFile();
	closeOutputFile();
}

void n2t::FileManager::setPath(const path& p) {
	_inputPath = _outputPath = _interPath = p;
	if (isFile(p)) {
		_outputPath.replace_extension(".asm");
		_interPath.replace_extension(".ic");
	}
	else if (isDirectory(p)) {
		_outputPath /= p.filename().replace_extension(".asm");
		_interPath /= p.filename().replace_extension(".ic");
	}
	else throw ("Input " + _inputPath.string() + " is not a valid file or directory.");
}

void n2t::FileManager::getFiles(void) {
	if (isDirectory(_inputPath)) {
		int count = countFiles(_inputPath);
		if (count < 1) throw ("The input directory is empty.");
		int i = 0;
		for (const path& entry : std::filesystem::directory_iterator(_inputPath)) {
			if (!isFile(entry) || !isVm(entry)) continue;
			std::cout << "Found file " << entry.filename() << '.' << std::endl;
			_files.push_back(entry);
			i++;
		}
		if (i > 0) _files.shrink_to_fit();
		else throw ("Couldn't find any .vm files in the input directory.");
	}
	else if (isFile(_inputPath)) {
		if (!isVm(_inputPath)) throw ("Wrong input extension.");
		_files.push_back(_inputPath);
	}
	_foundSys = std::any_of(_files.begin(), _files.end(), [](const auto& f) { return f.filename() == "Sys.vm"; });
}

bool n2t::FileManager::openNextFile(void) {
	closeInputFile();
	if (_files.empty()) return false;
	_inputFileName = _files.front().stem().string();
	_inputFile.open(_files.front());
	if (_inputFile.fail()) {
		std::cerr << "Failed to open input file " << _files.front().filename() << std::endl;
		return false;
	}
	_files.pop_front();
	return true;
}

bool n2t::FileManager::openInterFile(void) {
	_interFile.open(_interPath);
	if (_interFile.fail()) {
		std::cerr << "Failed to open intermediate file " << _interPath << std::endl;
		return false;
	}
	return true;
}

bool n2t::FileManager::openOutputFile(void) {
	_outputFile.open(_outputPath);
	if (_outputFile.fail()) {
		std::cerr << "Failed to open output file " << _outputPath << std::endl;
		return false;
	}
	return true;
}

void n2t::FileManager::closeInputFile(void) {
	if (_inputFile.is_open()) _inputFile.close();
	_inputFile.clear();
}

void n2t::FileManager::closeInterFile(void) {
	if (_interFile.is_open()) _interFile.close();
	_interFile.clear();
}

void n2t::FileManager::closeOutputFile(void) {
	if (_outputFile.is_open()) _outputFile.close();
	_outputFile.clear();
}

path& n2t::FileManager::inputPath(void) {
	return _inputPath;
}

path& n2t::FileManager::outputPath(void) {
	return _outputPath;
}

path& n2t::FileManager::interPath(void) {
	return _interPath;
}

std::ifstream& n2t::FileManager::readInputFile(void) {
	return _inputFile;
}

void n2t::FileManager::writeOutputFile(const std::string& s) {
	_outputFile << s;
}

void n2t::FileManager::writeInterFile(const std::string& s) {
	_interFile << s;
}

std::string& n2t::FileManager::inputFileName(void) {
	return _inputFileName;
}

bool n2t::FileManager::includesOS(void) {
	return _foundSys;
}

bool n2t::FileManager::isDirectory(const path& p) const {
	try {
		if (std::filesystem::exists(p) && is_directory(p)) return true;
	}
	catch (std::filesystem::filesystem_error& e)
	{
		std::cerr << e.what() << std::endl;
	}
	return false;
}

bool n2t::FileManager::isFile(const path& p) const {
	try {
		if (std::filesystem::exists(p) && std::filesystem::is_regular_file(p))
			return true;
	}
	catch (std::filesystem::filesystem_error& e) {
		std::cerr << e.what() << std::endl;
	}
	return false;
}

bool n2t::FileManager::isVm(const path& p) const {
	try {
		if (p.has_extension() && p.extension() == ".vm") return true;
	}
	catch (std::filesystem::filesystem_error& e) {
		std::cerr << e.what() << std::endl;
	}
	return false;
}

int n2t::FileManager::countFiles(const path& p) const {
	using fp = bool (*)(const path&);
	return std::count_if(std::filesystem::directory_iterator(p), std::filesystem::directory_iterator{}, (fp)std::filesystem::is_regular_file);
}