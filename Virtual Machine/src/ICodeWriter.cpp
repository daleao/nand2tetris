#include "../incl/ICodeWriter.hpp"
#include "../incl/maps.hpp"

n2t::ICodeWriter::ICodeWriter(const std::string& program, bool writeComments)
	: _buffer(""), _programName(program), _writeComments(writeComments), _writeCall(false), _writeReturn(false), _ROMIndex(0) {
	srand(std::time(NULL));
	_labelIndex = rand() % 1000;
	_globalCallLabel = _programName + ".global$CALL" + std::to_string(++_labelIndex);
	_globalReturnLabel = _programName + ".global$RET" + std::to_string(++_labelIndex);
	write("// .......... PROGRAM: ", _programName, " ..........");
	if (_writeComments) write("\n// (", _ROMIndex, "): bootstrap code");
	write("@255\nD=A\n@SP\nM=D");
	_ROMIndex += 4;
}

void n2t::ICodeWriter::writeCommand(Instruction& inst) {
	if (_writeComments and inst.opcode != "module") write("\n// (", _ROMIndex, "): ", inst.toString());
	switch (keyFromIOpcode.at(inst.opcode)) {
	case 0: { writeLoad(inst.what, inst.how); break; }
	case 1: { writePush(); break; }
	case 2: { writePushC(inst.what); break; }
	case 3: { writePop(); break; }
	case 4: { writeCopy(inst.where); break; }
	case 5: { writeCopyC(inst.what, inst.where); break; }
	case 6: { writeOp(inst.how); break; }
	case 7: { writeOpM(inst.where, inst.how); break; }
	case 8: { writeAdd(inst.what, inst.how); break; }
	case 9: { writeSub(inst.what, inst.how); break; }
	case 10: { writeAddM(inst.where, inst.how); break; }
	case 11: { writeSubM(inst.where, inst.how); break; }
	case 12: { writeAnd(inst.what); break; }
	case 13: { writeOr(inst.what); break; }
	case 14: { writeAndM(inst.where); break; }
	case 15: { writeOrM(inst.where); break; }
	case 16: { writeNand(inst.what); break; }
	case 17: { writeNor(inst.what); break; }
	case 18: { writeNandM(inst.where); break; }
	case 19: { writeNorM(inst.where); break; }
	case 20: { writeComp(inst.what); break; }
	case 21: { writeLabel(inst.what); break; }
	case 22: { writeJump(inst.where); break; }
	case 23: { writeIf(inst.what, inst.where); break; }
	case 24: { writeFunction(inst.what); break; }
	case 25: { writeCall(inst.what); break; }
	case 26: { writeReturn(); break; }
	case 27: { setModule(inst.what); break; }
	}
}

template <typename head_t, typename... tail_t>
void n2t::ICodeWriter::write(const head_t& head, const tail_t&... tail) {
	_oss << head;
	write(tail...);
}

void n2t::ICodeWriter::write(void) { _oss << "\n"; }

void n2t::ICodeWriter::callSysInit(void) {
	write("@Sys.init\n0;JMP");
	_ROMIndex += 2;
}

void n2t::ICodeWriter::endWriting(void) {
	if (_writeCall) writeGlobalCall();
	if (_writeReturn) writeGlobalReturn();
	_oss << std::endl;
	std::istringstream iss(_oss.str());
	std::ostringstream oss;
	for (std::string line; std::getline(iss, line);) {
		if (line[0] != ml::any_of('(', '/')) oss << "\t";
		oss << line << "\n";
	}
	_buffer = oss.str();
}

std::string& n2t::ICodeWriter::buffer(void) {
	return _buffer;
}

void n2t::ICodeWriter::writeLoad(const std::string& what, const std::string& how) {
	Address from(what);
	if (from.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(from.seg);
		write('@', from.seg);
		if (from.idx == 0) { write("A=M"); _ROMIndex++; }
		else if (from.idx == 1) { write("A=M+1"); _ROMIndex++; }
		else if (from.idx == 2) { write("A=M+1\nA=A+1"); _ROMIndex += 2; }
		else { write("D=M\n@", from.idx, "\nA=D+A"); _ROMIndex += 3; }
		_ROMIndex++;
	}
	else if (from.seg == ml::any_of("ptr", "tmp")) {
		int base_idx = from.seg == "ptr" ? 3 : 5;
		write("@R", base_idx + from.idx);
		_ROMIndex++;
	}
	else if (from.seg == "stc") { write('@', _moduleName, '.', from.idx); _ROMIndex++; }
	else if (from.seg == "ret") { write("@R13"); _ROMIndex++; }
	if (!how.empty()) {
		uint16_t constant = 0;
		if (how.front() == '+') {
			constant += (uint16_t)std::stoi(how.substr(1, std::string::npos));
			if (constant == 0) { write("D=M"); _ROMIndex++; }
			else if (constant == 1) { write("D=M+1"); _ROMIndex++; }
			else if (constant == 2) { write("D=M+1\nD=D+1"); _ROMIndex += 2; }
			else if (constant == UINT16_MAX) { write("D=M-1"); _ROMIndex++; }
			else if (constant == UINT16_MAX - 1) { write("D=M-1\nD=D-1"); _ROMIndex += 2; }
			else if (constant == (UINT16_MAX + 1) / 2) { write("D=M\n@", (uint16_t)(0 - constant - 1), "\nA=A+1\nD=D-A"); _ROMIndex += 4; }
			else if (constant > (UINT16_MAX + 1) / 2) { write("D=M\n@", (uint16_t)(0 - constant), "\nD=D-A"); _ROMIndex += 3; }
			else { write("D=M\n@", constant, "\nD=D+A"); _ROMIndex += 3; }
		}
		else if (how.back() == '-') {
			constant += (uint16_t)std::stoi(how.substr(0, how.find('-')));
			if (constant == 0) { write("D=-M"); _ROMIndex++; }
			else if (constant == 1) { write("D=1\nD=D-M"); _ROMIndex += 2; }
			else if (constant == UINT16_MAX) { write("D=!M"); _ROMIndex++; }
			else if (constant == UINT16_MAX - 1) { write("D=!M\nD=D-1"); _ROMIndex += 2; }
			else if (constant == (UINT16_MAX + 1) / 2) { write("D=-M\n@", (uint16_t)(0 - constant - 1), "A=A+1\nD=D-A"); _ROMIndex += 4; }
			else if (constant > (UINT16_MAX + 1) / 2) { write("D=-M\n@", (uint16_t)(0 - constant), "\nD=D-A"); _ROMIndex += 3; }
			else { write("D=M\n@", constant, "\nD=A-D"); _ROMIndex += 3; }
		}
	}
	else { write("D=M"); _ROMIndex++; }
}

void n2t::ICodeWriter::writePush(void) { write("@SP\nAM=M+1\nM=D"); _ROMIndex += 3;}

void n2t::ICodeWriter::writePushC(const std::string& what) {
	uint16_t constant = std::stoi(what);
	int16_t signed_constant = (int16_t)constant;
	int16_t absolute_value = abs(signed_constant);
	if (absolute_value <= 2) {
		write("@SP\nAM=M+1");
		if (constant == 0) { write("MD=0"); _ROMIndex++; }
		else if (absolute_value == 1) { signed_constant > 0 ? write("MD=1") : write("MD=-1"); _ROMIndex++; }
		else if (absolute_value == 2) { signed_constant > 0 ? write("M=1\nMD=M+1") : write("M=-1\nMD=M-1"); _ROMIndex += 2; }
		_ROMIndex += 2;
	}
	else {
		if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant) - 1, "\nA=A+1\nD=-A"); _ROMIndex += 3; }
		else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=-A"); _ROMIndex += 2; }
		else { write('@', constant, "\nD=A"); _ROMIndex += 2; }
		write("@SP\nAM=M+1\nM=D");
		_ROMIndex += 3;
	}
}

void n2t::ICodeWriter::writePop(void) { write("@SP\nAM=M-1\nA=A+1\nD=M"); _ROMIndex += 4; }

void n2t::ICodeWriter::writeCopy(const std::string& where) {
	Address at(where);
	if (at.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(at.seg);
		if (at.idx > 7) { write("@R13\nM=D"); _ROMIndex += 2; }
		write('@', at.seg); _ROMIndex++;
		if (at.idx == 0) { write("A=M\nM=D"); _ROMIndex += 2; }
		else if (at.idx <= 7) {
			write("A=M+1");
			for (int i = 1; i < at.idx; i++) { write("A=A+1"); _ROMIndex++; }
			write("M=D");
			_ROMIndex += 2;
		}
		else { write("D=M\n@", at.idx, "\nD=D+A\n@R13\nD=D+M\nA=D-M\nMD=D-A"); _ROMIndex += 7; }
	}
	else if (at.seg == ml::any_of("ptr", "tmp")) {
		int base_idx = at.seg == "ptr" ? 3 : 5;
		write("@R", base_idx + at.idx, "\nM=D");
		_ROMIndex += 2;
	}
	else if (at.seg == "stc") { write('@', _moduleName, '.', at.idx, "\nM=D"); _ROMIndex += 2; }
	else if (at.seg == "ret") { write("@R13\nM=D"); _ROMIndex += 2; }
}

void n2t::ICodeWriter::writeCopyC(const std::string& what, const std::string& where) {
	Address at(where);
	uint16_t constant = std::stoi(what);
	int16_t signed_constant = (int16_t)constant;
	int16_t absolute_value = abs(signed_constant);
	if (at.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(at.seg);
		if (at.idx <= 2 and absolute_value <= 2) {
			write('@', at.seg);
			if (at.idx == 0) { write("A=M"); _ROMIndex++; }
			else if (at.idx == 1) { write("A=M+1"); _ROMIndex++; }
			else if (at.idx == 2) { write("A=M+1\nA=A+1"); _ROMIndex += 2; }
			if (constant == 0) { write("MD=0"); _ROMIndex++; }
			else if (absolute_value == 1) { signed_constant > 0 ? write("MD=1") : write("MD=-1"); _ROMIndex++; }
			else if (absolute_value == 2) { signed_constant > 0 ? write("M=1\nMD=M+1") : write("M=-1\nMD=M-1"); _ROMIndex += 2; }
			_ROMIndex++;
		}
		else if (at.idx <= 3 and absolute_value > 2) {
			if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=-A"); _ROMIndex += 3; }
			else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=-A"); _ROMIndex += 2; }
			else { write('@', constant, "\nD=A"); _ROMIndex += 2; }
			write('@', at.seg);
			if (at.idx == 0) { write("A=M"); _ROMIndex++; }
			else if (at.idx == 1) { write("A=M+1"); _ROMIndex++; }
			else if (at.idx == 2) { write("A=M+1\nA=A+1"); _ROMIndex += 2; }
			else if (at.idx == 3) { write("A=M+1\nA=A+1\nA=A+1"); _ROMIndex += 3; }
			write("M=D");
			_ROMIndex += 2;
		}
		else if (at.idx > 2 and absolute_value <= 3) {
			write('@', at.seg, "\nD=M\n@", at.idx, "\nA=D+A");
			if (constant == 0) { write("MD=0"); _ROMIndex++; }
			else if (absolute_value == 1) { signed_constant > 0 ? write("MD=1") : write("MD=-1"); _ROMIndex++; }
			else if (absolute_value == 2) { signed_constant > 0 ? write("M=1\nMD=M+1") : write("M=-1\nMD=M-1"); _ROMIndex += 2; }
			else if (absolute_value == 3) { signed_constant > 0 ? write("M=1\nM=M+1\nMD=M+1") : write("M=-1\nM=M-1\nMD=M-1"); _ROMIndex += 3; }
			_ROMIndex += 4;
		}
		else { write('@', at.seg, "\nD=M\n@", at.idx, "\nD=D+A\n@", constant, "\nD=D+A\nA=D-A\nMD=D-A"); _ROMIndex += 8; }
	}
	else {
		if (absolute_value > 2) {
			if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "A=A+1\nD=-A"); _ROMIndex += 3; }
			else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "D=-A"); _ROMIndex += 2; }
			else { write('@', constant, "\nD=A"); _ROMIndex += 2; }
		}
		if (at.seg == ml::any_of("ptr", "tmp")) {
			int base_idx = at.seg == "ptr" ? 3 : 5;
			write("@R", base_idx + at.idx);
			_ROMIndex++;
		}
		else if (at.seg == "stc") { write('@', _moduleName, '.', at.idx); _ROMIndex++; }
		if (constant == 0) { write("MD=0"); _ROMIndex++; }
		else if (absolute_value == 1) { signed_constant > 0 ? write("M=1") : write("MD=-1"); _ROMIndex++; }
		else if (absolute_value == 2) { signed_constant > 0 ? write("M=1\nM=M+1") : write("M=-1\nMD=M-1"); _ROMIndex += 2; }
		else { write("M=D"); _ROMIndex++; }
	}
}

void n2t::ICodeWriter::writeOp(const std::string& how) {
	uint16_t constant = 0;
	if (!how.empty()) {
		if (how.front() == '+') constant += (uint16_t)std::stoi(how.substr(1, std::string::npos));
		else if (how.back() == '-') constant += (uint16_t)std::stoi(how.substr(0, how.find('-')));
	}
	int16_t signed_constant = (int16_t)constant;
	int16_t absolute_value = abs(signed_constant);
	if (how.front() == '+') {
		if (constant > 0) {
			if (absolute_value == 1) { signed_constant > 0 ? write("D=D+1") : write("D=D-1"); _ROMIndex++; }
			else if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=D-A"); _ROMIndex += 3; }
			else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=D-A"); _ROMIndex += 2; }
			else { write('@', constant, "\nD=D+A"); _ROMIndex += 2; }
		}
	}
	else if (how.back() == '-') {
		if (constant == 0) { write("D=-D"); _ROMIndex++; }
		else if (constant == UINT16_MAX) { write("D=!D"); _ROMIndex++; }
		else if (constant == UINT16_MAX - 1) { write("D=!D\nD=D-1"); _ROMIndex += 2; }
		else if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nA=-A\nD=A-D"); _ROMIndex += 4; }
		else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nA=-A\nD=A-D"); _ROMIndex += 3; }
		else { write('@', constant, "\nD=D-A"); _ROMIndex += 2; }
	}
}

void n2t::ICodeWriter::writeOpM(const std::string& where, const std::string& how) {
	Address at(where);
	uint16_t constant = 0;
	if (!how.empty()) {
		if (how.front() == '+') constant += (uint16_t)std::stoi(how.substr(1, std::string::npos));
		else if (how.back() == '-') constant += (uint16_t)std::stoi(how.substr(0, how.find('-')));
	}
	int16_t signed_constant = (int16_t)constant;
	int16_t absolute_value = abs(signed_constant);
	if (at.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(at.seg);
		if (how.front() == '+' and constant > 0) {
			if ((at.idx == 0 and absolute_value <= 2) or (at.idx + absolute_value <= 3)) {
				write('@', at.seg);
				if (at.idx == 0) { write("A=M"); _ROMIndex++; }
				else {
					write("A=M+1");
					for (int i = 1; i < at.idx; i++) { write("A=A+1"); _ROMIndex++; }
					_ROMIndex++;
				}
				for (int i = 0; i < absolute_value - 1; i++) { signed_constant > 0 ? write("M=M+1") : write("M=M-1"); _ROMIndex++; }
				signed_constant > 0 ? write("MD=M+1") : write("MD=M-1");
				_ROMIndex += 2;
			}
			else if (at.idx >= 3 and absolute_value <= 6) {
				write('@', at.seg, "\nD=M\n@", at.idx, "\nA=D+A");
				for (int i = 0; i < absolute_value - 1; i++) { signed_constant > 0 ? write("M=M+1") : write("M=M-1"); _ROMIndex++; }
				signed_constant > 0 ? write("MD=M+1") : write("MD=M-1");
				_ROMIndex += 5;
			}
			else if (at.idx <= 6 and absolute_value >= 3) {
				if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=-A"); _ROMIndex += 3; }
				else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=-A"); _ROMIndex += 2; }
				else { write('@', constant, "\nD=A"); _ROMIndex += 2; }
				write('@', at.seg);
				if (at.idx == 0) { write("A=M"); _ROMIndex++; }
				else {
					write("A=M+1");
					for (int i = 1; i < at.idx; i++) { write("A=A+1"); _ROMIndex++; }
					_ROMIndex++;
				}
				write("MD=D+M");
				_ROMIndex += 2;
			}
			else {
				write('@', at.seg, "\nD=M\n@", at.idx, "\nD=D+A\n@R14\nM=D");
				if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=-A"); _ROMIndex += 3; }
				else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=-A"); _ROMIndex += 2; }
				else { write('@', constant, "\nD=A"); _ROMIndex += 2; }
				write("@R14\nA=M\nMD=D+M");
				_ROMIndex += 9;
			}
		}
		else if (how.back() == '-') {
			if (at.idx == 0 or at.idx == 1) {
				write('@', at.seg);
				at.idx == 0 ? write("A=M") : write("A=M+1");
				if (constant == 0) { write("MD=-M"); _ROMIndex++; }
				else if (constant == 1) { write("M=-M\nMD=M-1"); _ROMIndex += 2; }
				else if (constant == UINT16_MAX) { write("MD=!M"); _ROMIndex++; }
				else if (constant == UINT16_MAX - 1) { write("M=!M\nMD=M-1"); _ROMIndex += 2; }
				_ROMIndex += 2;
			}
			else if (at.idx == 2) {
				write('@', at.seg, "\nA=M+1\nA=A=1");
				if (constant == 0) { write("MD=-M"); _ROMIndex++; }
				else if (constant == UINT16_MAX) { write("MD=!M"); _ROMIndex++; }
				_ROMIndex += 3;
			}
			else if (at.idx >= 3 and (-7 <= signed_constant or signed_constant <= 5)) {
				write('@', at.seg, "\nD=M\n@", at.idx, "\nA=D+A");
				if (constant == 0) { write("MD=-M"); _ROMIndex++; }
				else if (constant == UINT16_MAX - 1) { write("MD=!M"); _ROMIndex++; }
				else if (signed_constant > 0) {
					write("M=-M");
					for (int i = 0; i < signed_constant - 1; i++) { write("M=M+1"); _ROMIndex++; }
					write("MD=M+1");
					_ROMIndex += 2;
				}
				else if (signed_constant < 0) {
					write("M=!M");
					for (int i = signed_constant + 2; i < 0; i++) { write("M=M-1"); _ROMIndex++; }
					write("MD=M-1");
					_ROMIndex += 2;
				}
				_ROMIndex += 4;
			}
			else if (at.idx <= 6 and absolute_value >= 3) {
				if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=-A"); _ROMIndex += 3; }
				else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=-A"); _ROMIndex += 2; }
				else { write('@', constant, "\nD=A"); _ROMIndex += 2; }
				write('@', at.seg);
				if (at.idx == 0) { write("A=M"); _ROMIndex++; }
				else {
					write("A=M+1");
					for (int i = 1; i < at.idx; i++) { write("A=A+1"); _ROMIndex++; }
					_ROMIndex++;
				}
				write("MD=D-M");
				_ROMIndex += 2;
			}
			else {
				write('@', at.seg, "\nD=M\n@", at.idx, "\nD=D+A\n@R14\nM=D");
				if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=-A"); _ROMIndex += 3; }
				else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=-A"); _ROMIndex += 2; }
				else { write('@', constant, "\nD=A"); _ROMIndex += 2; }
				write("@R14\nA=M\nMD=D+M");
				_ROMIndex += 9;
			}
		}
	}
	else {
		if (how.front() == '+' and constant > 0) {
			if (absolute_value > 2) {
				if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=-A"); _ROMIndex += 3; }
				else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=-A"); _ROMIndex += 2; }
				else { write('@', constant, "\nD=A"); _ROMIndex += 2; }
			}
			if (at.seg == ml::any_of("ptr", "tmp")) {
				int base_idx = at.seg == "ptr" ? 3 : 5;
				write("@R", base_idx + at.idx);
				_ROMIndex++;
			}
			else if (at.seg == "stc") { write('@', _moduleName, '.', at.idx); _ROMIndex++; }
			if (absolute_value == 1) { signed_constant > 0 ? write("MD=M+1") : write("MD=M-1"); _ROMIndex++; }
			else if (absolute_value == 2) { signed_constant > 0 ? write("M=M+1\nMD=M+1") : write("M=M=1\nMD=M-1"); _ROMIndex += 2; }
			else { write("MD=M+D"); _ROMIndex++; }
		}
		else if (how.back() == '-') {
			if (signed_constant < -2 and 1 < signed_constant) {
				if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=-A"); _ROMIndex += 3; }
				else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=-A"); _ROMIndex += 2; }
				else { write('@', constant, "\nD=A"); _ROMIndex++; }
			}
			if (at.seg == ml::any_of("ptr", "tmp")) {
				int base_idx = at.seg == "ptr" ? 3 : 5;
				write("@R", base_idx + at.idx);
				_ROMIndex++;
			}
			else if (at.seg == "stc") { write('@', _moduleName, '.', at.idx); _ROMIndex++; }
			if (constant == 0) { write("MD=-M"); _ROMIndex++; }
			else if (constant == 1) { write("M=-M\nMD=M+1"); _ROMIndex += 2; }
			else if (constant == UINT16_MAX) { write("MD=!M"); _ROMIndex++; }
			else if (constant == UINT16_MAX - 1) { write("M=!M\nMD=M-1"); _ROMIndex += 2; }
			else { write("MD=D-M"); _ROMIndex++; }
		}
	}
}

void n2t::ICodeWriter::writeAdd(const std::string& what, const std::string& how) {
	Address from(what);
	uint16_t constant = 0;
	if (!how.empty()) {
		if (how.front() == '+') constant += (uint16_t)std::stoi(how.substr(1, std::string::npos));
		else if (how.back() == '-') constant += (uint16_t)std::stoi(how.substr(0, how.find('-')));
	}
	int16_t signed_constant = (int16_t)constant;
	int16_t absolute_value = abs(signed_constant);
	if (from.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(from.seg);
		if ((from.idx <= 6 and absolute_value == 0) or (from.idx <= 5 and absolute_value == 1) or (from.idx <= 4 and absolute_value > 1)) {
			if (absolute_value == 1) { signed_constant > 0 ? write("D=D+1") : write("D=D-1"); _ROMIndex++; }
			else if (absolute_value > 1) {
				if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=D-A"); _ROMIndex += 3; }
				else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=D-A"); _ROMIndex += 2; }
				else { write('@', constant, "\nD=D+A"); _ROMIndex += 2; }
			}
			write('@', from.seg);
			if (from.idx == 0) { write("A=M"); _ROMIndex++; }
			else if (from.idx <= 6) {
				write("A=M+1");
				for (int i = 1; i < from.idx; i++) { write("A=A+1"); _ROMIndex++; }
				_ROMIndex++;
			}
			if (how.empty() or how.front() == '+') { write("D=D+M"); _ROMIndex++; }
			else if (how.back() == '-') { write("D=D-M"); _ROMIndex++; }
			_ROMIndex++;
		}
		else {
			if (absolute_value == 3) { signed_constant > 0 ? write("D=D+1") : write("D=D-1"); _ROMIndex++; }
			else if (absolute_value > 3) {
				if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=D-A"); _ROMIndex += 3; }
				else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=D-A"); _ROMIndex += 2; }
				else { write('@', constant, "\nD=D+A"); _ROMIndex += 2; }
			}
			write("@R13");
			if (constant > 0 and absolute_value <= 3) { signed_constant > 0 ? write("M=D+1") : write("M=D-1"); _ROMIndex++; }
			write('@', from.seg, "\nD=M\n@", from.idx, "\nA=D+A");
			if (1 < absolute_value and absolute_value <= 3) { signed_constant > 0 ? write("D=M+1") : write("D=M-1"); _ROMIndex++; }
			write("@R13");
			if (how.empty() or how.front() == '+') { write("D=D+M"); _ROMIndex++; }
			else if (how.back() == '-') { write("D=M-D"); _ROMIndex++; }
			_ROMIndex += 6;
		}
	}
	else if (from.seg == "pop") { write("@SP\nAM=M-1\nA=A+1\nD=D+M"); _ROMIndex += 4; }
	else {
		if (absolute_value == 1) { signed_constant > 0 ? write("D=D+1") : write("D=D-1"); _ROMIndex++; }
		else if (absolute_value > 1) {
			if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=D-A"); _ROMIndex += 3; }
			else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=D-A"); _ROMIndex += 2; }
			{ write('@', constant, "\nD=D+A"); _ROMIndex += 2; }
		}
		if (from.seg == ml::any_of("ptr", "tmp")) {
			int base_idx = from.seg == "ptr" ? 3 : 5;
			write("@R", base_idx + from.idx);
			_ROMIndex++;
		}
		else if (from.seg == "stc") { write('@', _moduleName, '.', from.idx); _ROMIndex++; }
		if (how.empty() or how.front() == '+') { write("D=D+M"); _ROMIndex++; }
		else if (how.back() == '-') { write("D=D-M"); _ROMIndex++; }
	}
}

void n2t::ICodeWriter::writeAddM(const std::string& where, const std::string& how) {
	Address at(where);
	uint16_t constant = 0;
	if (!how.empty()) {
		if (how.front() == '+') constant += (uint16_t)std::stoi(how.substr(1, std::string::npos));
		else if (how.back() == '-') constant += (uint16_t)std::stoi(how.substr(0, how.find('-')));
	}
	int16_t signed_constant = (int16_t)constant;
	int16_t absolute_value = abs(signed_constant);
	if (at.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(at.seg);
		if ((at.idx <= 10 and absolute_value == 0) or (at.idx <= 9 and absolute_value == 1) or (at.idx <= 8 and absolute_value > 1)) {
			if (absolute_value == 1) { signed_constant > 0 ? write("D=D+1") : write("D=D-1"); _ROMIndex++; }
			else if (absolute_value > 1) {
				if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=D-A"); _ROMIndex += 3; }
				else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=D-A"); _ROMIndex += 2; }
				else { write('@', constant, "\nD=D+A"); _ROMIndex += 2; }
			}
			write('@', at.seg);
			if (at.idx == 0) { write("A=M"); _ROMIndex++; }
			else if (at.idx <= 10) {
				write("A=M+1");
				for (int i = 1; i < at.idx; i++) { write("A=A+1"); _ROMIndex++; }
				_ROMIndex++;
			}
			if (how.empty() or how.front() == '+') { write("MD=D+M"); _ROMIndex++; }
			else if (how.back() == '-') { write("MD=D-M"); _ROMIndex++; }
			_ROMIndex++;
		}
		else {
			if (absolute_value > 3) {
				if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=D-A"); _ROMIndex += 3; }
				else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=D-A"); _ROMIndex += 2; }
				else { write('@', constant, "\nD=D+A"); _ROMIndex += 2; }
			}
			write("@R13");
			if (constant > 0 and absolute_value <= 3) { signed_constant > 0 ? write("M=D+1") : write("M=D-1"); _ROMIndex++; }
			write('@', at.seg, "\nD=M\n@", at.idx, "\nD=D+A\n@R14\nM=D\n@R13");
			if (1 < absolute_value and absolute_value <= 3) { signed_constant > 0 ? write("M=D+1") : write("M=D-1"); _ROMIndex++; }
			write("@R14\nA=M");
			if (how.front() == '+' or how.empty()) { write("MD=D+M"); _ROMIndex++; }
			else if (how.back() == '-') { write("MD=M-D"); _ROMIndex++; }
			if (absolute_value == 3) { signed_constant > 0 ? write("MD=D+1") : write("MD=D-1"); _ROMIndex++; }
			_ROMIndex += 10;
		}
	}
	else {
		if (absolute_value > 1) {
			if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=D-A"); _ROMIndex += 3; }
			else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=D-A"); _ROMIndex += 2; }
			else { write('@', constant, "\nD=D+A"); _ROMIndex += 2; }
		}
		if (at.seg == ml::any_of("ptr", "tmp")) {
			int base_idx = at.seg == "ptr" ? 3 : 5;
			write("@R", base_idx + at.idx);
			_ROMIndex++;
		}
		else if (at.seg == "stc") { write('@', _moduleName, '.', at.idx); _ROMIndex++; }
		if (how.empty() or how.front() == '+') { write("MD=D+M"); _ROMIndex++; }
		else if (how.back() == '-') { write("MD=D-M"); _ROMIndex++; }
		if (absolute_value == 1) { signed_constant > 0 ? write("MD=M+1") : write("MD=M-1"); _ROMIndex++; }
	}
}

void n2t::ICodeWriter::writeSub(const std::string& what, const std::string& how) {
	Address from(what);
	uint16_t constant = 0;
	if (!how.empty()) {
		if (how.front() == '+') constant += (uint16_t)std::stoi(how.substr(1, std::string::npos));
		else if (how.back() == '-') constant += (uint16_t)std::stoi(how.substr(0, how.find('-')));
	}
	int16_t signed_constant = (int16_t)constant;
	int16_t absolute_value = abs(signed_constant);
	if (from.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(from.seg);
		if ((from.idx <= 6 and absolute_value == 0) or (from.idx <= 5 and absolute_value == 1) or (from.idx <= 4 and absolute_value > 1)) {
			if (absolute_value == 1) { signed_constant > 0 ? write("D=D-1") : write("D=D+1"); _ROMIndex++; }
			else if (absolute_value > 1) {
				if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=D+A"); _ROMIndex += 3; }
				else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=D+A"); _ROMIndex += 2; }
				else { write('@', constant, "\nD=D-A"); _ROMIndex += 2; }
			}
			write('@', from.seg);
			if (from.idx == 0) { write("A=M"); _ROMIndex++; }
			else if (from.idx <= 6) {
				write("A=M+1");
				for (int i = 1; i < from.idx; i++) { write("A=A+1"); _ROMIndex++; }
				_ROMIndex++;
			}
			if (how.empty() or how.front() == '+') { write("D=D-M"); _ROMIndex++; }
			else if (how.back() == '-') { write("D=D+M"); _ROMIndex++; }
			_ROMIndex++;
		}
		else {
			if (absolute_value == 3) { signed_constant > 0 ? write("D=D-1") : write("D=D+1"); _ROMIndex++; }
			else if (absolute_value > 3) {
				if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=D+A"); _ROMIndex += 3; }
				else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=D+A"); _ROMIndex += 2; }
				else { write('@', constant, "\nD=D-A"); _ROMIndex += 2; }
			}
			write("@R13");
			if (constant > 0 and absolute_value <= 3) { signed_constant > 0 ? write("M=D-1") : write("M=D+1"); _ROMIndex++; }
			write('@', from.seg, "\nD=M\n@", from.idx, "\nA=D+A");
			if (1 < absolute_value and absolute_value <= 3) { signed_constant > 0 ? write("D=D+1") : write("D=D-1"); _ROMIndex++; }
			write("@R13");
			if (how.empty() or how.front() == '+') { write("D=M-D"); _ROMIndex++; }
			else if (how.back() == '-') { write("D=D+M"); _ROMIndex++; }
			_ROMIndex += 6;
		}
	}
	else if (from.seg == "pop") { write("@SP\nAM=M-1\nA=A+1\nD=M-D"); _ROMIndex += 4; }
	else {
		if (absolute_value == 1) { signed_constant > 0 ? write("D=D-1") : write("D=D+1"); _ROMIndex++; }
		else if (absolute_value > 1) {
			if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=D+A"); _ROMIndex += 3; }
			else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=D+A"); _ROMIndex += 2; }
			else { write('@', constant, "\nD=D-A"); _ROMIndex += 2; }
		}
		if (from.seg == ml::any_of("ptr", "tmp")) {
			int base_idx = from.seg == "ptr" ? 3 : 5;
			write("@R", base_idx + from.idx);
			_ROMIndex++;
		}
		else if (from.seg == "stc") { write('@', _moduleName, '.', from.idx); _ROMIndex++; }
		if (how.empty() or how.front() == '+') { write("D=D-M"); _ROMIndex++; }
		else if (how.back() == '-') { write("D=D+M"); _ROMIndex++; }
	}
}

void n2t::ICodeWriter::writeSubM(const std::string& where, const std::string& how) {
	Address at(where);
	uint16_t constant = 0;
	if (!how.empty()) {
		if (how.front() == '+') constant += (uint16_t)std::stoi(how.substr(1, std::string::npos));
		else if (how.back() == '-') constant += (uint16_t)std::stoi(how.substr(0, how.find('-')));
	}
	int16_t signed_constant = (int16_t)constant;
	int16_t absolute_value = abs(signed_constant);
	if (at.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(at.seg);
		if ((at.idx <= 10 and absolute_value == 0) or (at.idx <= 9 and absolute_value == 1) or (at.idx <= 8 and absolute_value > 1)) {
			if (absolute_value == 1) { signed_constant > 0 ? write("D=D-1") : write("D=D+1"); _ROMIndex++; }
			else if (absolute_value > 1) {
				if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=D+A"); _ROMIndex += 3; }
				else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=D+A"); _ROMIndex += 2; }
				else { write('@', constant, "\nD=D-A"); _ROMIndex += 2; }
			}
			write('@', at.seg);
			if (at.idx == 0) { write("A=M"); _ROMIndex++; }
			else if (at.idx <= 10) {
				write("A=M+1");
				for (int i = 1; i < at.idx; i++) { write("A=A+1"); _ROMIndex++; }
				_ROMIndex++;
			}
			if (how.empty() or how.front() == '+') { write("MD=D-M"); _ROMIndex++; }
			else if (how.back() == '-') { write("MD=D+M"); _ROMIndex++; }
			_ROMIndex++;
		}
		else {
			if (absolute_value > 3) {
				if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=D+A"); _ROMIndex += 3; }
				else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=D+A"); _ROMIndex += 2; }
				else { write('@', constant, "\nD=D-A"); _ROMIndex += 2; }
			}
			write("@R13");
			if (constant > 0 and absolute_value <= 3) { signed_constant > 0 ? write("M=D-1") : write("M=D+1"); _ROMIndex++; }
			write('@', at.seg, "\nD=M\n@", at.idx, "\nD=D+A\n@R14\nM=D\n@R13");
			if (1 < absolute_value and absolute_value <= 3) { signed_constant > 0 ? write("M=D-1") : write("M=D+1"); _ROMIndex++; }
			write("@R14\nA=M");
			if (how.front() == '+' or how.empty()) { write("MD=D-M"); _ROMIndex++; }
			else if (how.back() == '-') { write("MD=D+M"); _ROMIndex++; }
			if (absolute_value == 3) { signed_constant > 0 ? write("MD=D-1") : write("MD=D+1"); _ROMIndex++; }
			_ROMIndex += 10;
		}
	}
	else {
		if (absolute_value > 1) {
			if (constant == (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant - 1), "\nA=A+1\nD=D+A"); _ROMIndex += 3; }
			else if (constant > (UINT16_MAX + 1) / 2) { write('@', (uint16_t)(0 - constant), "\nD=D+A"); _ROMIndex += 2; }
			else { write('@', constant, "\nD=D-A"); _ROMIndex += 2; }
		}
		if (at.seg == ml::any_of("ptr", "tmp")) {
			int base_idx = at.seg == "ptr" ? 3 : 5;
			write("@R", base_idx + at.idx);
			_ROMIndex++;
		}
		else if (at.seg == "stc") { write('@', _moduleName, '.', at.idx); _ROMIndex++; }
		if (how.empty() or how.front() == '+') { write("MD=D-M"); _ROMIndex++; }
		else if (how.back() == '-') { write("MD=D+M"); _ROMIndex++; }
		if (absolute_value == 1) { signed_constant > 0 ? write("MD=M-1") : write("MD=M+1"); _ROMIndex++; }
	}
}

void n2t::ICodeWriter::writeAnd(const std::string& what) {
	Address from(what);
	if (from.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(from.seg);
		if (from.idx > 6) { write("@R13\nM=D"); _ROMIndex += 2; }
		write('@', from.seg);
		if (from.idx == 0) { write("A=M"); _ROMIndex++; }
		else if (from.idx <= 6) {
			write("A=M+1");
			for (int i = 1; i < from.idx; i++) { write("A=A+1"); _ROMIndex++; }
			_ROMIndex++;
		}
		else { write("D=M\n@", from.idx, "\nA=D+A\nD=M\n@R13"); _ROMIndex += 5; }
		_ROMIndex++;
	}
	else if (from.seg == ml::any_of("ptr", "tmp")) {
		int base_idx = from.seg == "ptr" ? 3 : 5;
		write("@R", base_idx + from.idx);
		_ROMIndex++;
	}
	else if (from.seg == "stc") { write('@', _moduleName, '.', from.idx); _ROMIndex++; }
	else if (from.seg == "pop") { write("@SP\nAM=M-1\nA=A+1"); _ROMIndex += 3; }
	else if (from.seg == "cte") {
		uint16_t constant = (uint16_t)from.idx;
		write('@', constant, "\nD=D&A");
		_ROMIndex += 2;
		return;
	}
	write("D=D&M");
	_ROMIndex++;
}

void n2t::ICodeWriter::writeAndM(const std::string& where) {
	Address at(where);
	if (at.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(at.seg);
		if (at.idx > 6) { write("@R13\nM=D"); _ROMIndex += 2; }
		write('@', at.seg);
		if (at.idx == 0) { write("A=M"); _ROMIndex++; }
		else if (at.idx <= 6) {
			write("A=M+1");
			for (int i = 1; i < at.idx; i++) { write("A=A+1"); _ROMIndex++; }
			_ROMIndex++;
		}
		else { write("D=M\n@", at.idx, "\nA=D+A\nD=M\n@R13"); _ROMIndex += 5; }
		_ROMIndex++;
	}
	else if (at.seg == ml::any_of("ptr", "tmp")) {
		int base_idx = at.seg == "ptr" ? 3 : 5;
		write("@R", base_idx + at.idx);
		_ROMIndex++;
	}
	else if (at.seg == "stc") { write('@', _moduleName, '.', at.idx); _ROMIndex++; }
	write("MD=D&M");
	_ROMIndex++;
}

void n2t::ICodeWriter::writeOr(const std::string& what) {
	Address from(what);
	if (from.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(from.seg);
		if (from.idx > 6) { write("@R13\nM=D"); _ROMIndex += 2; }
		write('@', from.seg);
		if (from.idx == 0) { write("A=M"); _ROMIndex++; }
		else if (from.idx <= 6) {
			write("A=M+1");
			for (int i = 1; i < from.idx; i++) { write("A=A+1"); _ROMIndex++; }
			_ROMIndex++;
		}
		else { write("D=M\n@", from.idx, "\nA=D+A\nD=M\n@R13"); _ROMIndex += 5; }
		_ROMIndex++;
	}
	else if (from.seg == ml::any_of("ptr", "tmp")) {
		int base_idx = from.seg == "ptr" ? 3 : 5;
		write("@R", base_idx + from.idx);
		_ROMIndex++;
	}
	else if (from.seg == "stc") { write('@', _moduleName, '.', from.idx); _ROMIndex++; }
	else if (from.seg == "pop") { write("@SP\nAM=M-1\nA=A+1"); _ROMIndex += 3; }
	else if (from.seg == "cte") {
		uint16_t constant = (uint16_t)from.idx;
		write('@', constant, "\nD=D|A");
		_ROMIndex += 2;
		return;
	}
	write("D=D|M");
	_ROMIndex++;
}

void n2t::ICodeWriter::writeOrM(const std::string& where) {
	Address at(where);
	if (at.seg == ml::any_of("lcl", "arg", "this", "that")) {
		toUpper(at.seg);
		if (at.idx > 6) { write("@R13\nM=D"); _ROMIndex += 2; }
		write('@', at.seg);
		if (at.idx == 0) { write("A=M"); _ROMIndex++; }
		else if (at.idx <= 6) {
			write("A=M+1");
			for (int i = 1; i < at.idx; i++) { write("A=A+1"); _ROMIndex++; }
			_ROMIndex++;
		}
		else { write("D=M\n@", at.idx, "\nA=D+A\nD=M\n@R13"); _ROMIndex += 5; }
		_ROMIndex++;
	}
	else if (at.seg == ml::any_of("ptr", "tmp")) {
		int base_idx = at.seg == "ptr" ? 3 : 5;
		write("@R", base_idx + at.idx);
		_ROMIndex++;
	}
	else if (at.seg == "stc") { write('@', _moduleName, '.', at.idx); _ROMIndex++; }
	write("MD=D|M");
	_ROMIndex++;
}

void n2t::ICodeWriter::writeNand(const std::string& what) {
	writeAnd(what);
	write("D=!D");
	_ROMIndex++;
}

void n2t::ICodeWriter::writeNandM(const std::string& where) {
	writeNand(where);
	write("MD=!M");
	_ROMIndex++;
}

void n2t::ICodeWriter::writeNor(const std::string& what) {
	writeOr(what);
	write("D=!D");
	_ROMIndex++;
}

void n2t::ICodeWriter::writeNorM(const std::string& where) {
	writeNor(where);
	write("MD=!M");
	_ROMIndex++;
}

void n2t::ICodeWriter::writeComp(std::string& what) {
	toUpper(what);
	const auto true_label = _moduleName + "$CMP_TRUE" + std::to_string(++_labelIndex);
	const auto end_label = _moduleName + "$CMP_END" + std::to_string(_labelIndex);
	write('@', true_label, "\nD;J", what, "\n@", end_label, "\nD=0;JMP\n(", true_label, ")\nD=-1\n(", end_label, ')');
	_ROMIndex += 5;
}

void n2t::ICodeWriter::writeLabel(const std::string& what) {
	write('(', what, ')');
}

void n2t::ICodeWriter::writeJump(const std::string& where) {
	write('@', where, "\n0;JMP");
	_ROMIndex += 2;
}

void n2t::ICodeWriter::writeIf(std::string& what, const std::string& where) {
	toUpper(what);
	write('@', where, "\nD;J", what);
	_ROMIndex += 2;
}

void n2t::ICodeWriter::writeFunction(const std::string& what) {
	auto pos = what.find(' ');
	auto function = what.substr(0, pos);
	auto nvars = std::stoi(what.substr(pos + 1, std::string::npos));
	setFunction(function);
	write('(', function, ')');
	if (nvars > 3) {
		const std::string loop = _functionName + "$INIT" + std::to_string(++_labelIndex);
		write('@', nvars, "\nD=A\n@R13\nM=D\n(", loop, ')');
		writePushC("0");
		write("@R13\nMD=M-1\n@", loop, "\nD;JNE");
		_ROMIndex += 8;
	}
	else for (int i = 0; i < nvars; i++) writePushC("0");
}

void n2t::ICodeWriter::writeCall(const std::string& what) {
	if (!_writeCall) _writeCall = true;
	auto pos = what.find(' ');
	auto function = what.substr(0, pos);
	auto nargs = std::stoi(what.substr(pos + 1, std::string::npos));
	const std::string return_label = _functionName + "$RET" + std::to_string(++_labelIndex);
	write('@', function, "\nD=A\n@R14\nM=D");
	if (nargs == 0) { write("@R15\nM=0"); _ROMIndex += 2; }
	else if (nargs == 1) { write("@R15\nM=1"); _ROMIndex += 2; }
	else if (nargs == 2) { write("@R15\nM=1\nM=M+1"); _ROMIndex += 3; }
	else { write('@', nargs, "\nD=A\n@R15\nM=D"); _ROMIndex += 4; }
	write('@', return_label, "\nD=A");
	write('@', _globalCallLabel, "\n0;JMP\n(", return_label, ')');
	_ROMIndex += 8;
}

void n2t::ICodeWriter::writeGlobalCall(void) {
	if (_writeComments) write("\n// (", _ROMIndex, "): global call subroutine");
	write('(', _globalCallLabel, ')');
	writePush();
	for (auto&& seg : { "LCL", "ARG", "THIS", "THAT" }) {
		write('@', seg, "\nD=M");
		writePush();
		_ROMIndex += 2;
	}
	write("D=A\n@LCL\nM=D+1\n@4\nD=D-A\n@R15\nD=D-M\n@ARG\nM=D\n@R14\nA=M\n0;JMP");
	_ROMIndex += 12;
}

void n2t::ICodeWriter::writeReturn(void) { 
	if (!_writeReturn) _writeReturn = true;
	write('@', _globalReturnLabel, "\n0;JMP");
	_ROMIndex += 2;
}

void n2t::ICodeWriter::writeGlobalReturn() {
	if (_writeComments) write("\n// (", _ROMIndex, "): global return subroutine");
	write('(', _globalReturnLabel, ')');
	write("@ARG\nD=M-1\n@SP\nM=D\n@LCL\nD=M\n@R14\nAM=D-1\nD=M\n@THAT\nM=D");
	for (auto&& seg : { "THIS", "ARG", "LCL" }) { write("@R14\nAM=M-1\nD=M\n@", seg, "\nM=D"); _ROMIndex += 5; }
	write("@R14\nA=M-1\nA=M\n0;JMP");
	_ROMIndex += 15;
}

void n2t::ICodeWriter::setModule(const std::string& module) {
	if (_writeComments) write("\n// ... Module: ", module, " ...");
	_moduleName = module;
}

void n2t::ICodeWriter::setFunction(const std::string& function) { _functionName = function; }

void n2t::ICodeWriter::toUpper(std::string& s) { std::for_each(s.begin(), s.end(), [](char& c) { c = ::toupper(c); }); }