#include "../incl/structs.hpp"
#include "../incl/maps.hpp"

n2t::Instruction::Instruction(void) : opcode(""), what(""), where(""), how("") {}

n2t::Instruction::Instruction(const std::string& s)
	: what(""), where(""), how("") {
	std::istringstream iss(s.substr(0, s.find('/')));
	std::istream_iterator<std::string> begin(iss), end;
	std::vector<std::string> tokens(begin, end);
	opcode = tokens[0];
	if (tokens.size() > 1) {
		what = segmentAbbreviations.contains(tokens[1]) ? segmentAbbreviations.at(tokens[1]) : tokens[1];
		if (opcode == ml::any_of("push", "pop", "function", "call")) what += ' ' + tokens[2];
	}
}

std::string n2t::Instruction::toString(void) const {
	std::ostringstream oss;
	oss << std::left << std::setw(10) << opcode;
	if (opcode == ml::any_of("push_d", "d_pop", "return")) return oss.str();
	if (!what.empty()) {
		if (opcode == ml::any_of("module", "function", "call")) {
			auto pos = what.find(' ');
			oss << '"' << what.substr(0, pos) << '"';
			if (opcode != "module") oss << ' ' << what.substr(pos + 1, std::string::npos);
		}
		else if (opcode == "--") oss << '(' << what << ')';
		else oss << what;
	}
	if (!where.empty()) {
		if (!what.empty()) {
			if (opcode == "jumpif_d") oss << " -> ";
			else oss << ", ";
		}
		if (opcode == "jump") oss << "-> ";
		oss << where;
	}
	if (!how.empty()) {
		if (opcode != "d_op") oss << ' ';
		oss << "[" << how << "]";
	}
	return oss.str();
}

void n2t::Instruction::operator=(const Instruction& other) {
	opcode = other.opcode;
	what = other.what;
	where = other.where;
	how = other.how;
}

bool n2t::Instruction::operator==(const Instruction& other) const {
	return opcode == other.opcode and what == other.what and where == other.where and how == other.how;
}

n2t::Address::Address(void) : seg(""), idx(NULL) {}

n2t::Address::Address(const std::string& s) {
	std::istringstream iss(s);
	iss >> seg;
	iss >> idx;
}

bool n2t::Address::isEmpty(void) const { return seg.empty() and idx == NULL; }

void n2t::Address::operator=(const Address& other) {
	seg = other.seg;
	idx = other.idx;
}

bool n2t::Address::operator==(const Address& other) const { return seg == other.seg and idx == other.idx; }

std::ostream& n2t::operator<<(std::ostream& os, const Address& op) {
	os << op.seg << ' ' << op.idx;
	return os;
}


n2t::Operand::Operand(void) : seg(""), idx(-1) {}

n2t::Operand::Operand(const std::string& s) {
	std::istringstream iss(s);
	iss >> seg;
	if (!iss.eof()) iss >> idx;
	else idx = -1;
}

n2t::Operand::Operand(int i) : seg(""), idx(i) {}

bool n2t::Operand::isEmpty(void) const { return seg.empty() and idx == -1; }

void n2t::Operand::operator=(const Operand& other) {
	seg = other.seg;
	idx = other.idx;
}

bool n2t::Operand::operator==(const Operand& other) const { return seg == other.seg and idx == other.idx; }

std::ostream& n2t::operator<<(std::ostream& os, const Operand& op) {
	os << op.seg;
	if (op.idx != -1) {
		if (!op.seg.empty()) os << ' ';
		os << op.idx;
	}
	return os;
}