# VM Translator
### Nand2Tetris Part 2 - Weeks 07-08

C++ implementation of a full optimizing translator for the Hack VM language with varying levels of complexity.

### Level 0 (No optimization):

VM commands are translated "verbose"; that is, exactly according to their textbook descriptions in *The Elements of Computing Systems*. Intended for learning only as the generated code size often exceeds the 32k ROM limit of the Hack architecture. Commands of each type always follow the same pattern and only ever use single-register assignments. All operations are carried out in the stack; binary operations always begin by decrementing the stack pointer twice and end by incrementing it once. Unary operations begin by decrementing the stack pointer once and again end by incrementing it once.

Ex. 

    // push constant 1
	@1
	D=A     // load the value into D
	@SP
	A=M     // access the stack
	M=D     // store the value
	@SP
	M=M+1   // increment the stack pointer

	// neg
	@SP
	M=M-1   // decrement the stack pointer
	A=M     // access the stack
	M=-M    // negate the value
	@SP
	M=M+1   // increment the stack pointer

### Level 1 (Blind optimization):

VM commands are optimized individually as much as possible by making full use of the available elementary hack operations:
*  Use multiple assignment to change and dereference pointers in a single line;
*  All operations involving constants 0, 1 or -1 are carried out without needing to load a value into the A register;
*  Certain address or value assignments can be shortcutted using increments;

Ex.

    // push constant 0
    @SP
    AM=M+1  // increment and dereference in a single line
    MD=0    // set to zero without first loading into A
    
    // pop local 2
    @LCL
    A=M+1
    A=A+1   // access index 2 faster using increments rather than first loading into A and then adding

By far the biggest change, however, is to the behavior of the stack pointer. The number of increments and decrements can be reduced by a great deal by definig the stack pointer such that it always points to the last occupied register instead of the next empty register. This ensures shorter push commands in exchange for longer pop commands, yielding an overall net reduction in code size and speed due to the much higher frequency of pushes. Moreover, one decrement in each arithmetic operation is eliminated by pre-emptively loading resulting values into the D register. By incorporating both these changes, all binary operations need only decrement the stack pointer once, and never increment it. Unary operations don't change the stack pointer at all.

Ex. 
    
    // push constant 1
	@SP
	AM=M+1
	MD=1    // set value and pre-emptively load it to D

	// neg
	MD=-M   // negate the value and pre-emptively load it to D
(70% reduction over level 0)

Note that, as a result, the stack pointer is initialized to 255 (instead of 256). 

### Level 2 (Interpreted optimization):

VM commands are optimized collecively by interpreting their context. This is done by employing an Interpreter class which converts traditional VM commands into new intermediate commands. The new commands are kept in a running queue and can be changed or removed if judged redundant. The key observation to be made is that, given context, most operations can be done directly in-memory, without ever touching the stack.

As an example, consider the ordinary push command:

    // push local 1
    @LCL
    A=M+1   // find the source address
    D=M     // load the value into D
    @SP
    AM=M+1  // increment and dereference the stack pointer
    MD=D    // copy the value
    
Notice how a single push can be broken down into several sub commands. In particular, a push always involves loading a value into the D register and then pushing that value into the stack. However, the latter step is often redundant:

    // push local 1
    ...
    @SP
    AM=M+1
    MD=D
    
    // pop this 0
    AM=M-1  // dereference and decrement the stack pointer
    A=A+1
    D=M     // load the value into D
    @THIS
    A=M     // find the dest address
    M=D     // copy the value

Here, the value of D was pushed onto the stack only to be immediately popped. These two operations in sequence cancel each other out. It makes sense, then, to break down pushes and pops into subcommands in order to avoid redundancies.

The interpreter does exactly that. A single push is split into a load to the D register followed by a push to the stack:

    push local 1    -->     d_load lcl 1 + push_d

Similarly, a pop is split into a pop to the D register followed by a copy to an address. If a pop immediately follows a push, however, it will instead cancel out the push. This can be visualized as follows:

    1. Command received: push local 1
    Queue:
        d_load lcl 1
        push_d
        
    2. Command received: pop this 0
    Queue:
        d_load lcl 1
        copy_d this 0

This behavior is not limited to pops. Consider an addition:

    1. Command received: push argument 0
    Queue:
        d_load arg 0
        push_d
    
    2. Command received: push local 2
    Queue:
        d_load arg 0
        push_d
        d_load lcl 2
        push_d
        
    3. Command received: add
    Queue:
        d_load arg 0
        d_add lcl 2
        push_d
        
In this case, the second load operation was changed to an addition. This context-aware addition operation will find the specified address and immediately add to D the value of M. Putting it all together:

    // push argument 0          // d_load arg 0
    @ARG                        @ARG
    A=M                         A=M
    D=M                         D=M
    @SP
    AM=M+1
    MD=D
    
    // push local 2             // d_add lcl 2
    @LCL                        @LCL
    A=M+1                       A=M+1
    A=A+1                       A=A+1
    D=M                         D=D+M
    @SP
    AM=M+1
    MD=D
    
    // add
    @SP
    AM=M-1
    MD=D+M
    
    // pop this 0               // copy_d this 0
    @SP                         @THIS
    AM=M-1                      A=M
    A=A+1                       MD=D
    @THIS
    A=M
    MD=D
(54% reduction)

If the copy destination matches any of the load sources, the entire sequence can be carried out directly in the M register:

	// push argument 0          // d_load arg 0
	@ARG                        @ARG
	A=M                         A=M
	D=M                         D=M
	@SP
	AM=M+1
	MD=D

	// push local 2             // m_add lcl 2
	@LCL                        @LCL
	A=M+1                       A=M+1
	A=A+1                       A=A+1
	D=M                         MD=D+M
	@SP
	AM=M+1
	MD=1

	// add
	@SP
	AM=M-1
	MD=M+D

	// pop local 2
	@SP
	AM=M-1
	A=A+1
	D=M
	@LCL
	A=M+1
	A=A+1
	MD=D
(71% reduction)

Note that the command prefix indicates which register will be occupied by the result. On the other hand, a command suffix (such as "push_d") indicates which register will be consumed for the operation.

The Interpreter also eliminates pushing constants [almost] completely. Instead, any modification by a constant is incorporated as an optional argument:

    1. Command received: push local 0
    Queue:
        d_load lcl 0
        push_d
    
    2. Command received: push constant 2
    Queue:
        d_load lcl 0
        push_d
        d_loadc 2
        push_d
    
    3. Command received: sub
    Queue:
        d_load lcl 0 [+65534]
        push_d
        
Note that 65534 represents -2 in the two's compliment standard. The only context where constants **must** be pushed is when they serve as arguments to a function:

    Queue:
        d_load lcl 0
        push_d
        d_loadc 2
        push_d
        
    Command received: call Math.multiply 2
    Queue:
        d_load lcl 0
        push_d
        push_c 2
        call "Math.multiply" 2

Other optimizations include eliminating redundant jumps by making if-goto operations aware of the jump condition:

	push x                      d_load x
	push y                      d_sub y
	eq                          jumpif_d eq -> LABEL
	if-goto LABEL

Rather than evaluate the comparison and jump on the resulting boolean, the Interpreter splits the subtraction out from the comparison and jumps immediately based on the result and the desired condition. Notice that, as result, all conditional jumps are essentially comparing with zero (as does the Hack assembly).

These are just a few examples of interpreted optimizations. The full list is too long to detail. Use the flag "--save-icode" or "-si" to save the generated intermediate commands to a separate file.

### Level 3 (+ Relative indexing):

Even further optimization can be achieved by using relative address indexing.

Ex.

	// d_load lcl 0             // d_load lcl 0
	@LCL                        @LCL
	A=M                         A=M
	D=M                         D=M
	
	// d_add lcl 1              // d_add up 1
	@LCL                        A=A+1
	A=M+1                       D=D+M
	D=D+M
	
Notice how consecutive operations to the same memory segment (in this case LCL) don't need to reference the segment pointer each time. If the indices are high, we can potentially save many lines by simply incrementing or decrementing the A register rather than getting the address from the segment pointer.

